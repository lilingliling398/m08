package com.example.liling_examen2_2_practica;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

public class MainActivity extends AppCompatActivity {
    ListView listView;
    List<String> cursos;
    boolean isActiveActionMode = false;
    ActionMode actionMode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        //hook
        listView = findViewById(R.id.listView);

        cursos.add("1JSIM");
        cursos.add("2JSIM");
        cursos.add("1HISX");
        cursos.add("2HISX");
        cursos.add("1HISM");
        cursos.add("2HISM");
        cursos.add("1HIAW");
        cursos.add("2HIAW");
        cursos.add("1JISM");
        cursos.add("2JISM");
        cursos.add("1HISX");
        cursos.add("2HISX");
        cursos.add("1WIAM");
        cursos.add("2WIAM");
        cursos.add("1WIAW");
        cursos.add("2WIAW");

        CustomAdapter customAdapter = new CustomAdapter();

        listView.setAdapter(customAdapter);

        listView.setChoiceMode(AbsListView.CHOICE_MODE_MULTIPLE_MODAL);

        AbsListView.MultiChoiceModeListener modeListener = new AbsListView.MultiChoiceModeListener() {
            @Override
            public void onItemCheckedStateChanged(ActionMode mode, int position, long id, boolean checked) {
                //nada
            }

            @Override
            public boolean onCreateActionMode(ActionMode mode, Menu menu) {
                if (actionMode != null) {
                    return false;
                }
                MenuInflater menuInflater = mode.getMenuInflater();
                menuInflater.inflate(R.menu.my_menu, menu);
                mode.setTitle("Choose your opcion");
                isActiveActionMode = true;
                return true;
            }

            @Override
            public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
                return false;
            }

            @Override
            public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.item1:
                        Toast.makeText(MainActivity.this, item + " " + item.getItemId(), Toast.LENGTH_SHORT).show();
                        return true;
                    case R.id.item2:
                        Toast.makeText(MainActivity.this, item + " " + item.getItemId(), Toast.LENGTH_SHORT).show();
                        return true;
                    default:
                        return false;
                }

            }

            @Override
            public void onDestroyActionMode(ActionMode mode) {
                actionMode = null;
                isActiveActionMode = false;
            }
        };
        listView.setMultiChoiceModeListener(modeListener);

    }


    private class CustomAdapter extends BaseAdapter {
        @Override
        public int getCount() {
            return cursos.size();
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        //!!!!!!!
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View view = getLayoutInflater().inflate(R.layout.list_view, null);
            //se pasan los parametros de view.xml
            TextView textView = view.findViewById(R.id.textRow);
            CheckBox checkBox = view.findViewById(R.id.checkBox);

            textView.setText(cursos.get(position));
            if (isActiveActionMode) {
                checkBox.setVisibility(View.VISIBLE);
            }
            return view;
        }
    }
}