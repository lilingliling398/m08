package com.example.examenuf2_practica_liling;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    private Toolbar toolbar;
    private ListView listView;

    ArrayList<Gym> gymList = new ArrayList<>();

    //context menu
    @Override
    public boolean onContextItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.itemReserve:
                AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
                int index = info.position;

                Intent intent = new Intent(MainActivity.this, BookActivity.class);
                intent.putExtra(BookActivity.BOOK_TYPE ,gymList.get(index).getClassType());
                intent.putExtra(BookActivity.BOOK_TRAINER ,gymList.get(index).getTrainer());
                intent.putExtra(BookActivity.BOOK_DESCRIPTION ,gymList.get(index).getDescription());
                intent.putExtra(BookActivity.BOOK_KCAL ,gymList.get(index).getkCal());
                intent.putExtra(BookActivity.BOOK_DURATION ,gymList.get(index).getDuration());
                intent.putExtra(BookActivity.BOOK_ROOM ,gymList.get(index).getRoom());
                intent.putExtra(BookActivity.BOOK_DATE ,gymList.get(index).getDate());
                intent.putExtra(BookActivity.BOOK_TIME ,gymList.get(index).getTime());
                intent.putExtra(BookActivity.BOOK_IMAGES ,gymList.get(index).getImages());



                startActivity(intent);
                return true;
            case R.id.itemInfo:
                Toast.makeText(this, "Info", Toast.LENGTH_SHORT).show();
                return true;
            case R.id.itemShare:
                Toast.makeText(this, "Share", Toast.LENGTH_SHORT).show();
                return true;
            default:
                Toast.makeText(this, "---", Toast.LENGTH_SHORT).show();
                return super.onContextItemSelected(item);
        }

    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        getMenuInflater().inflate(R.menu.my_context_menu, menu);
        menu.setHeaderTitle("Choose an option");
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //hooks
        toolbar = findViewById(R.id.toolbar);
        listView = findViewById(R.id.listView);

        toolbar.setTitle("RunApp");
        toolbar.setNavigationIcon(R.drawable.ic_baseline_menu_24);
        setSupportActionBar(toolbar);

        initData();
        CustomAdapter adapter = new CustomAdapter();
        listView.setAdapter(adapter);
        registerForContextMenu(listView);
    }

    //tooblar
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.my_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.item1:
                Toast.makeText(this, "Perfil", Toast.LENGTH_SHORT).show();
                return true;
            case R.id.item2:
                Toast.makeText(this, "Shop", Toast.LENGTH_SHORT).show();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    class CustomAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return gymList.size();
        }

        @Override
        public Object getItem(int position) {
            return gymList.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View view = getLayoutInflater().inflate(R.layout.my_row, null);
            //hooks
            ImageView image = view.findViewById(R.id.mImage);
            TextView type = view.findViewById(R.id.mType);
            TextView trainer = view.findViewById(R.id.mTrainer);
            TextView date = view.findViewById(R.id.mDate);
            TextView time = view.findViewById(R.id.mTime);

            image.setImageResource(gymList.get(position).getImage0());
            type.setText(gymList.get(position).getClassType());
            trainer.setText(gymList.get(position).getTrainer());
            date.setText(gymList.get(position).getDate());
            time.setText(gymList.get(position).getTime());
            return view;
        }
    }

    private void initData(){
        int[] imageSpinning = {R.drawable.spinning1, R.drawable.spinning2, R.drawable.spinning3, R.drawable.spinning4, R.drawable.spinning5, R.drawable.spinning6};
        int[] imageFitness = {R.drawable.fitness1, R.drawable.fitness2, R.drawable.fitness3, R.drawable.fitness4, R.drawable.fitness5, R.drawable.fitness6};
        int[] imageKombat = {R.drawable.kombat1, R.drawable.kombat2, R.drawable.kombat3};
        gymList.add(new Gym("Spinning", "Peter K.", "10/10/2020", "9:00 am", R.drawable.spinning0, imageSpinning, "Class of Spinning", "200 kCal", "1 hour", "2A"));
        gymList.add(new Gym("Fitness", "Anna B", "10/10/2020", "10:00 am", R.drawable.fitness0, imageFitness, "Class of Fitness", "100 kCal", "2 hours", "1B"));
        gymList.add(new Gym("Kombat", "Johnny T", "10/10/2020", "10:30 am", R.drawable.kombat0, imageKombat, "Class of Kombat", "150 kCal", "1.5 hours", "3A"));
        gymList.add(new Gym("Spinning", "Peter K.", "10/10/2020", "9:00 am", R.drawable.spinning0, imageSpinning, "Class of Spinning", "200 kCal", "1 hour", "2A"));
        gymList.add(new Gym("Fitness", "Anna B", "10/10/2020", "10:00 am", R.drawable.fitness0, imageFitness, "Class of Fitness", "100 kCal", "2 hours", "1B"));
        gymList.add(new Gym("Kombat", "Johnny T", "10/10/2020", "10:30 am", R.drawable.kombat0, imageKombat, "Class of Kombat", "150 kCal", "1.5 hours", "3A"));
        gymList.add(new Gym("Spinning", "Peter K.", "10/10/2020", "9:00 am", R.drawable.spinning0, imageSpinning, "Class of Spinning", "200 kCal", "1 hour", "2A"));
        gymList.add(new Gym("Fitness", "Anna B", "10/10/2020", "10:00 am", R.drawable.fitness0, imageFitness, "Class of Fitness", "100 kCal", "2 hours", "1B"));
        gymList.add(new Gym("Kombat", "Johnny T", "10/10/2020", "10:30 am", R.drawable.kombat0, imageKombat, "Class of Kombat", "150 kCal", "1.5 hours", "3A"));

    }
}