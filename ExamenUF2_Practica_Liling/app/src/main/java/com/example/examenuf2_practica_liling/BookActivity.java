package com.example.examenuf2_practica_liling;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

public class BookActivity extends AppCompatActivity {
    static final String BOOK_TYPE = "examenuf2_practica_liling.TYPE";
    static final String BOOK_TRAINER = "examenuf2_practica_liling.TRAINER";
    static final String BOOK_DESCRIPTION = "examenuf2_practica_liling.DESCRIPTION";
    static final String BOOK_KCAL = "examenuf2_practica_liling.KCAL";
    static final String BOOK_DURATION = "examenuf2_practica_liling.DURATION";
    static final String BOOK_ROOM = "examenuf2_practica_liling.ROMM";
    static final String BOOK_DATE = "examenuf2_practica_liling.DATE";
    static final String BOOK_TIME = "examenuf2_practica_liling.TIME";
    static final String BOOK_IMAGES = "examenuf2_practica_liling.IMAGES";

    String sType;
    String sTrainer;
    String sDescription;
    String sKcal;
    String sDuration;
    String sRoom;
    String sDate;
    String sTime;
    int[] sImages;

    private ImageView imageView;
    private TextView type;
    private TextView trainer;
    private TextView description;
    private TextView kcal;
    private TextView duration;
    private TextView room;
    private TextView date;
    private TextView time;
    private GridView gridView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book);

        //hooks
        imageView = findViewById(R.id.bImage);
        type = findViewById(R.id.bType);
        trainer = findViewById(R.id.bTrainer);
        description = findViewById(R.id.bDescrip);
        kcal = findViewById(R.id.bKcal);
        duration = findViewById(R.id.bDuration);
        room = findViewById(R.id.bRoom);
        date = findViewById(R.id.bDate);
        time = findViewById(R.id.bTime);
        gridView = findViewById(R.id.gridview);


        sType = getIntent().getStringExtra(BOOK_TYPE);
        sTrainer = getIntent().getStringExtra(BOOK_TRAINER);
        sDescription = getIntent().getStringExtra(BOOK_DESCRIPTION);
        sKcal= getIntent().getStringExtra(BOOK_KCAL);
        sDuration= getIntent().getStringExtra(BOOK_DURATION);
        sRoom= getIntent().getStringExtra(BOOK_ROOM);
        sDate = getIntent().getStringExtra(BOOK_DATE);
        sTime = getIntent().getStringExtra(BOOK_TIME);
        sImages = getIntent().getIntArrayExtra(BOOK_IMAGES);

        imageView.setImageResource(sImages[0]);
        type.setText(sType);
        trainer.setText(sTrainer);
        description.setText(sDescription);
        kcal.setText(sKcal);
        duration.setText(sDuration);
        room.setText(sRoom);
        date.setText(sDate);
        time.setText(sTime);
        ImagesAdapter adapter = new ImagesAdapter();
        gridView.setAdapter(adapter);
    }

    class ImagesAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return sImages.length;
        }

        @Override
        public Object getItem(int position) {
            return sImages[position];
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View view = getLayoutInflater().inflate(R.layout.my_row2, null);
            //hooks
            ImageView imageView = view.findViewById(R.id.imageView2);
            imageView.setImageResource(sImages[position]);
            return view;
        }
    }
}