package com.example.examenuf1_practica_liling;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class DashboardActivity extends AppCompatActivity {
    private CardView cardView1;
    private CardView cardView2;
    private CardView cardView3;
    private CardView cardView4;
    private CardView cardView5;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);

        //hooks
        cardView1 = findViewById(R.id.c1);
        cardView2 = findViewById(R.id.c2);
        cardView3 = findViewById(R.id.c3);
        cardView4 = findViewById(R.id.c4);
        cardView5 = findViewById(R.id.c5);

        cardView1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String productTitol = "Refugi Josep Maria Blanc";
                int imageTop = R.drawable.foto1;

                intetReserva(productTitol, imageTop, 1);
            }
        });

        cardView2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String productTitol = "Refugi Cap de Llauset";
                int imageTop = R.drawable.foto2;

                intetReserva(productTitol, imageTop, 2);
            }
        });

        cardView3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String productTitol = "Refugi Ventosa i Clavell";
                int imageTop = R.drawable.foto3;

                intetReserva(productTitol, imageTop, 3);
            }
        });

        cardView4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String productTitol = "Refugi Amitges";
                int imageTop = R.drawable.foto4;

                intetReserva(productTitol, imageTop, 4);
            }
        });

        cardView5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String productTitol = "Refugi Josep Maria Montfort";
                int imageTop = R.drawable.foto5;

                intetReserva(productTitol, imageTop, 5);
            }
        });
    }

    public void intetReserva(String productTitol, int imageTop, int num){
        //seleccionaros
        Intent intent = new Intent(DashboardActivity.this, ReservaActivity.class);
        intent.putExtra(ReservaActivity.PRODUCT_TITOL, productTitol);
        intent.putExtra(ReservaActivity.PRODUCT_IMAGE, imageTop);
        intent.putExtra(ReservaActivity.NUM, num);

        startActivity(intent);
    }
}