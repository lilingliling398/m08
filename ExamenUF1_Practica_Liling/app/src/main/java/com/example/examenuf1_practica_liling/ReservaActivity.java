package com.example.examenuf1_practica_liling;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SwitchCompat;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Calendar;

public class ReservaActivity extends AppCompatActivity {
    static String PRODUCT_TITOL = "com.example.examenuf1_practica_liling.PRODUCT_TITOL";
    static String PRODUCT_IMAGE = "com.example.examenuf1_practica_liling.PRODUCT_IMAGE";
    static String NUM = "com.example.examenuf1_practica_liling.NUM";

    private ArrayList<String> refugis = new ArrayList<>();
    private Spinner spinner;
    private ImageView imageView;
    private int imagenTop;
    private int num;

    private RadioGroup radioGroup;
    private RadioButton radioBt1;
    private RadioButton radioBt2;
    private RadioButton radioBt3;
    private RadioButton radioBt4;
    private SwitchCompat switchCompat;


    DatePickerDialog pickerEntrada;
    DatePickerDialog pickerSortida;
    EditText textDateEntrada;
    EditText textDateSortida;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reserva);

        //hooks
        spinner = findViewById(R.id.spinner);
        imageView = findViewById(R.id.imagen);

        radioGroup = findViewById(R.id.radioGroup);
        radioBt1 = findViewById(R.id.radioButton);
        radioBt2 = findViewById(R.id.radioButton2);
        radioBt3 = findViewById(R.id.radioButton3);
        radioBt4 = findViewById(R.id.radioButton4);
        switchCompat = findViewById(R.id.switch1);

        textDateEntrada = findViewById(R.id.editTextDate);
        textDateSortida = findViewById(R.id.editTextDate2);

        //spinner
        refugis.add(0, "Selecciona Refugi");
        refugis.add("Refugi Josep Maria Blanc");
        refugis.add("Refugi Cap de Llauset");
        refugis.add("Refugi Ventosa i Clavell");
        refugis.add("Refugi Amitges");
        refugis.add("Refugi Josep Maria Montfort");

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_activated_1, refugis);
        spinner.setAdapter(dataAdapter);

        //get intent
        imagenTop = getIntent().getIntExtra(PRODUCT_IMAGE, 0);
        num = getIntent().getIntExtra(NUM, 0);

        imageView.setImageResource(imagenTop);
        spinner.setSelection(num);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String selectedCondition = parent.getItemAtPosition(position).toString();
                Toast.makeText(ReservaActivity.this, selectedCondition, Toast.LENGTH_SHORT).show();

                switch (selectedCondition) {
                    case "Refugi Josep Maria Blanc":
                        imagenTop = R.drawable.foto1;
                        break;
                    case "Refugi Cap de Llauset":
                        imagenTop = R.drawable.foto2;
                        break;
                    case "Refugi Ventosa i Clavell":
                        imagenTop = R.drawable.foto3;
                        break;
                    case "Refugi Amitges":
                        imagenTop = R.drawable.foto4;
                        break;
                    case "Refugi Josep Maria Montfort":
                        imagenTop = R.drawable.foto5;
                        break;
                }
                imageView.setImageResource(imagenTop);
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                Toast.makeText(ReservaActivity.this, "Nothing Selected", Toast.LENGTH_SHORT).show();
            }
        });

        //radiobutton
        radioBt1.setEnabled(false);
        radioBt2.setEnabled(false);
        radioBt3.setEnabled(false);
        radioBt4.setEnabled(false);
        radioBt4.setChecked(true);

        //switch
        if (switchCompat != null) {
            switchCompat.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (isChecked) {
                        Toast.makeText(ReservaActivity.this, "Activat", Toast.LENGTH_SHORT).show();
                        radioBt1.setEnabled(true);
                        radioBt2.setEnabled(true);
                        radioBt3.setEnabled(true);
                        radioBt4.setEnabled(true);

                    } else {
                        Toast.makeText(ReservaActivity.this, "Desactivat", Toast.LENGTH_SHORT).show();
                        radioBt1.setEnabled(false);
                        radioBt2.setEnabled(false);
                        radioBt3.setEnabled(false);
                        radioBt4.setEnabled(false);
                        radioBt1.setChecked(false);
                        radioBt2.setChecked(false);
                        radioBt3.setChecked(false);
                        radioBt4.setChecked(true);
                    }
                }

            });

        }

        //fecha
        textDateEntrada.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar cldr = Calendar.getInstance();
                int day = cldr.get(Calendar.DAY_OF_MONTH);
                int month = cldr.get(Calendar.MONTH);
                int year = cldr.get(Calendar.YEAR);

                pickerEntrada = new DatePickerDialog(ReservaActivity.this, android.R.style.Theme_Holo_Light_Dialog, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        textDateEntrada.setText(dayOfMonth + "/" + (monthOfYear + 1) + "/" + year);
                    }
                }, year, month, day);
                pickerEntrada.show();

            }
        });

        textDateSortida.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar cldr = Calendar.getInstance();
                int day = cldr.get(Calendar.DAY_OF_MONTH);
                int month = cldr.get(Calendar.MONTH);
                int year = cldr.get(Calendar.YEAR);

                pickerSortida = new DatePickerDialog(ReservaActivity.this, android.R.style.Theme_Holo_Light_Dialog, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        textDateSortida.setText(dayOfMonth + "/" + (monthOfYear + 1) + "/" + year);
                    }
                }, year, month, day);
                pickerSortida.show();

            }
        });

    }
}