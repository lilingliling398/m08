package com.example.examenuf1_practica_liling;

import androidx.appcompat.app.AppCompatActivity;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.app.ActivityOptions;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Pair;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    private final int SPLASH_SCREEN = 4000;
    private ImageView logo1;
    private ImageView logo2;
    private TextView text1;
    private TextView text2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //hooks
        logo1 = findViewById(R.id.logo1);
        logo2 = findViewById(R.id.logo2);
        text1 = findViewById(R.id.texto1);
        text2 = findViewById(R.id.texto2);

        ObjectAnimator aLogo1 = ObjectAnimator.ofFloat(logo1, "translationX", -1000f, 0f);
        aLogo1.setDuration(2000);
        ObjectAnimator aText1 = ObjectAnimator.ofFloat(text1, "translationX", 1000f, 0f);
        aText1.setDuration(2000);
        ObjectAnimator aText2 = ObjectAnimator.ofFloat(text2, "translationX", 1000f, 0f);
        aText2.setDuration(2000);
        ObjectAnimator aLogo2 = ObjectAnimator.ofFloat(logo2, "alpha", 0f, 1f);
        aLogo2.setStartDelay(2000);
        aLogo2.setDuration(2000);

        AnimatorSet animationSet = new AnimatorSet();
        animationSet.playTogether(aLogo1, aLogo2, aText1, aText2);
        animationSet.start();

        //animacion entre activities
        new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(MainActivity.this, DashboardActivity.class);

                Pair[] pairs = new Pair[2];
                pairs[0] = new Pair<View,String>(logo1,"logo_origen");
                pairs[1] = new Pair<View,String>(text1,"text_origen");

                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                    ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation(MainActivity.this, pairs);
                    startActivity(intent,options.toBundle());
                }

            }
        },SPLASH_SCREEN);



    }

}