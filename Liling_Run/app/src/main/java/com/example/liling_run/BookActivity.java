package com.example.liling_run;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class BookActivity extends AppCompatActivity {
    private Toolbar mytoolbar;
    private GridView gridViewImager;
    private ArrayList<Parcelable> gym;
    private Gym gymSelect;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book);

        //hook
        mytoolbar = findViewById(R.id.myToolbar);
        setSupportActionBar(mytoolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        /*CustomAdapter customAdapter = new CustomAdapter();

        //pasarle el custom adapter(no array adapter)
        gridViewImager.setAdapter(customAdapter);*/


        registerForContextMenu(gridViewImager);


        Intent i = getIntent();


        gym = i.getParcelableArrayListExtra(MainActivity.PRODUCT_GYM);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.my_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.itemUser:
                Toast.makeText(this, "User", Toast.LENGTH_SHORT).show();
                return true;
            case R.id.itemShop:
                Toast.makeText(this, "Shop", Toast.LENGTH_SHORT).show();
                return true;
            case android.R.id.home:
                //intents
                Toast.makeText(this, "Home", Toast.LENGTH_SHORT).show();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }

    /*private class CustomAdapter extends BaseAdapter {
        @Override
        public int getCount() {
            return length;
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View view = getLayoutInflater().inflate(R.layout.list_view, null);

            ImageView imageView = view.findViewById(R.id.imageViewPerfil);
            TextView textView = view.findViewById(R.id.textViewClass);

            imageView.setImageResource(imagesPerfil[position]);
            textView.setText(listNom.get(position));
            return view;
        }
    }*/
}