package com.example.liling_run;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    private Toolbar mytoolbar;
    private int[] imagesPerfil = {R.drawable.spinning0, R.drawable.fitness0, R.drawable.kombat0, R.drawable.spartan0, R.drawable.zumba0,
                            R.drawable.spinning0, R.drawable.fitness0, R.drawable.kombat0, R.drawable.spartan0, R.drawable.zumba0,
                            R.drawable.spinning0, R.drawable.fitness0, R.drawable.kombat0, R.drawable.spartan0, R.drawable.zumba0};
    private List<String> listNom = new ArrayList<>();
    private ListView listViewImagen;
    private List<Gym> gymList;
    private String typeGym;



    public static String PRODUCT_INDEX = "ccom.example.liling_run.PRODUCT_INDEX";
    public static String PRODUCT_GYM = "com.example.liling_run.PRODUCT_GYM";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        //hook
        mytoolbar = findViewById(R.id.myToolbar);
        setSupportActionBar(mytoolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        listViewImagen = findViewById(R.id.listView);


        listNom.add("Spinning");
        listNom.add("Fitness");
        listNom.add("Kombat");
        listNom.add("Spartan");
        listNom.add("Zumba");
        listNom.add("Spinning");
        listNom.add("Fitness");
        listNom.add("Kombat");
        listNom.add("Spartan");
        listNom.add("Zumba");
        listNom.add("Spinning");
        listNom.add("Fitness");
        listNom.add("Kombat");
        listNom.add("Spartan");
        listNom.add("Zumba");

        CustomAdapter customAdapter = new CustomAdapter();


        listViewImagen.setAdapter(customAdapter);


        registerForContextMenu(listViewImagen);

        //gym
        int [] spiImages = {R.drawable.spinning1,R.drawable.spinning2, R.drawable.spinning3, R.drawable.spinning4, R.drawable.spinning5, R.drawable.spinning6};
        Gym spinning = new Gym("Spinning", R.drawable.spinning0, spiImages);

        int[] komImages = {R.drawable.kombat1, R.drawable.kombat2, R.drawable.kombat3};
        Gym komnat = new Gym("Kombat", R.drawable.kombat0, komImages);

        gymList.add(spinning);
        gymList.add(komnat);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.my_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.itemUser:
                Toast.makeText(this, "User", Toast.LENGTH_SHORT).show();
                return true;
            case R.id.itemShop:
                Toast.makeText(this, "Shop", Toast.LENGTH_SHORT).show();
                return true;
            case android.R.id.home:
                //intents
                Toast.makeText(this, "Home", Toast.LENGTH_SHORT).show();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }

    private class CustomAdapter extends BaseAdapter {
        @Override
        public int getCount() {
            return imagesPerfil.length;
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View view = getLayoutInflater().inflate(R.layout.list_view, null);

            ImageView imageView = view.findViewById(R.id.imageViewPerfil);
            TextView textView = view.findViewById(R.id.textViewClass);


            imageView.setImageResource(imagesPerfil[position]);
            typeGym =listNom.get(position);
            textView.setText(listNom.get(position));
            return view;
        }
    }


    //content menu
    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        menu.setHeaderTitle("Choose an option");
        menu.add(0, 1, 1, "Reserve");
        menu.add(0, 2, 2, "Info");
        menu.add(0, 3, 3, "Share");
    }


    @Override
    public boolean onContextItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case 1:
                Toast.makeText(this, "Reserve", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(MainActivity.this, BookActivity.class);

                startActivity(intent);
                return true;
            case 2:
                Toast.makeText(this, "Info", Toast.LENGTH_SHORT).show();
                return true;
            case 3:
                Toast.makeText(this, "Share", Toast.LENGTH_SHORT).show();
                return true;
            default:
                return super.onContextItemSelected(item);
        }
    }

}