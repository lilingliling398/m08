package com.example.liling_act38_menu_action_mode;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.view.ActionMode;

import android.content.Context;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    private ListView listView;
    ArrayList<String> cursos = new ArrayList<>();

    CustomAdapterMulti customAdapterMulti;
    boolean isActiveActionMode = false;
    private ActionMode mActionMode;

    ArrayList<String> itemsSelected = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //hooks
        listView = findViewById(R.id.listView);
        cursos.add("1JSIM");
        cursos.add("2JSIM");
        cursos.add("3HISX");
        cursos.add("4HISX");
        cursos.add("5HISM");
        cursos.add("6HISM");
        cursos.add("7HIAW");
        cursos.add("8HIAW");
        cursos.add("9JISM");
        cursos.add("10JISM");

        customAdapterMulti = new CustomAdapterMulti(this, cursos);
        listView.setAdapter(customAdapterMulti);

        listView.setChoiceMode(AbsListView.CHOICE_MODE_MULTIPLE_MODAL);
        AbsListView.MultiChoiceModeListener modeListener = new AbsListView.MultiChoiceModeListener() {
            @Override
            public void onItemCheckedStateChanged(android.view.ActionMode mode, int position, long id, boolean checked) {
            }

            @Override
            public boolean onCreateActionMode(android.view.ActionMode mode, Menu menu) {
                if (mActionMode != null) {
                    return false;
                }
                MenuInflater menuInflater = mode.getMenuInflater();
                menuInflater.inflate(R.menu.my_menu, menu);
                mode.setTitle("Choose your option");
                isActiveActionMode=true;
                return true;
            }

            @Override
            public boolean onPrepareActionMode(android.view.ActionMode mode, Menu menu) {
                return false;
            }

            @Override
            public boolean onActionItemClicked(android.view.ActionMode mode, MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.item1:
                        //delete
                        cursos.removeAll(itemsSelected);
                        customAdapterMulti.notifyDataSetChanged();
                        Toast.makeText(MainActivity.this, item.getTitle() + ": ", Toast.LENGTH_SHORT).show();
                        return true;
                    case R.id.item2:
                        //share
                        Toast.makeText(MainActivity.this, item.getTitle() + " ", Toast.LENGTH_SHORT).show();
                        return true;
                    default:
                        return false;
                }
            }

            @Override
            public void onDestroyActionMode(android.view.ActionMode mode) {
                mActionMode = null;
                isActiveActionMode= false;
                itemsSelected.clear();
            }
        };
        listView.setMultiChoiceModeListener(modeListener);


    }

    class CustomAdapterMulti extends BaseAdapter {
        private Context context;
        private ArrayList<String> cursos; 

        public CustomAdapterMulti(Context context, ArrayList<String> cursos) {
            this.context = context;
            this.cursos = cursos;
        }

        @Override
        public int getCount() {
            return cursos.size();
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View view = getLayoutInflater().inflate(R.layout.row_data_multi, null);
            TextView textRow = view.findViewById(R.id.textRow);
            CheckBox checkBox = view.findViewById(R.id.checkBox);

            textRow.setText(cursos.get(position));
            if (isActiveActionMode) {
                checkBox.setVisibility(View.VISIBLE);
            }

            checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (isChecked){
                        itemsSelected.add(textRow.getText().toString());
                    }else {
                        itemsSelected.remove(textRow.getText().toString());
                    }
                }
            });

            return view;
        }
    }


}