package com.example.liling_29c_radiobuttons;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SwitchCompat;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private RadioGroup radioGroup1;
    private RadioGroup radioGroup2;
    private RadioGroup radioGroup3;
    private RadioButton radioBt1;
    private RadioButton radioBt2;
    private RadioButton radioBt3;
    private RadioButton radioBt4;
    private RadioButton radioBt5;
    private RadioButton radioBt6;
    private RadioButton radioBt7;
    private RadioButton radioBt8;
    private RadioButton radioBt9;
    private SwitchCompat switchCompat;
    private Button buttonCheck;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Hooks
        radioGroup1 = (RadioGroup) findViewById(R.id.radioGroup1);
        radioGroup2 = (RadioGroup) findViewById(R.id.radioGroup2);
        radioGroup3 = (RadioGroup) findViewById(R.id.radioGroup3);
        radioBt1 = (RadioButton) findViewById(R.id.radioButton1);
        radioBt2 = (RadioButton) findViewById(R.id.radioButton2);
        radioBt3 = (RadioButton) findViewById(R.id.radioButton3);
        radioBt4 = (RadioButton) findViewById(R.id.radioButton4);
        radioBt5 = (RadioButton) findViewById(R.id.radioButton5);
        radioBt6 = (RadioButton) findViewById(R.id.radioButton6);
        radioBt7 = (RadioButton) findViewById(R.id.radioButton7);
        radioBt8 = (RadioButton) findViewById(R.id.radioButton8);
        radioBt9 = (RadioButton) findViewById(R.id.radioButton9);
        switchCompat = (SwitchCompat) findViewById(R.id.switch1);
        buttonCheck = findViewById(R.id.button);

        radioGroup1.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                int radioSelected = radioGroup1.getCheckedRadioButtonId();
                switch (radioSelected) {
                    case R.id.radioButton1:
                        Toast.makeText(getApplicationContext(), "Radio Button 1", Toast.LENGTH_SHORT).show();
                        break;
                    case R.id.radioButton2:
                        Toast.makeText(getApplicationContext(), "Radio Button 2", Toast.LENGTH_SHORT).show();
                        break;
                    case R.id.radioButton3:
                        Toast.makeText(getApplicationContext(), "Radio Button 3", Toast.LENGTH_SHORT).show();
                        break;
                    default:
                        break;
                }
            }
        });


        radioGroup2.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                int radioSelected = radioGroup2.getCheckedRadioButtonId();
                switch (radioSelected) {
                    case R.id.radioButton4:
                        Toast.makeText(getApplicationContext(), "Radio Button 4", Toast.LENGTH_SHORT).show();
                        break;
                    case R.id.radioButton5:
                        Toast.makeText(getApplicationContext(), "Radio Button 5", Toast.LENGTH_SHORT).show();
                        break;
                    case R.id.radioButton6:
                        Toast.makeText(getApplicationContext(), "Radio Button 6", Toast.LENGTH_SHORT).show();
                        break;
                    default:
                        break;
                }

            }
        });


        if (switchCompat != null) {
            switchCompat.setChecked(true);
            switchCompat.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                    for (int i = 0; i < radioGroup3.getChildCount(); i++) {
                        radioGroup3.getChildAt(i).setActivated(false);
                    }

                    if (isChecked) {
                        Toast.makeText(MainActivity.this, "Enable", Toast.LENGTH_SHORT).show();
                        for (int i = 0; i < radioGroup3.getChildCount(); i++) {
                            radioGroup3.getChildAt(i).setEnabled(true);
                        }
                    }else {
                        Toast.makeText(MainActivity.this, "Disable", Toast.LENGTH_SHORT).show();
                        for (int i = 0; i < radioGroup3.getChildCount(); i++) {
                            radioGroup3.getChildAt(i).setEnabled(false);
                            radioGroup3.getChildAt(i).setActivated(false);
                        }
                        radioGroup3.clearCheck();
                    }
                }
            });
        }


        radioGroup3.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                int radioSelected = radioGroup3.getCheckedRadioButtonId();
                switch (radioSelected) {
                    case R.id.radioButton7:
                        Toast.makeText(getApplicationContext(), "Radio Button 7", Toast.LENGTH_SHORT).show();
                        break;
                    case R.id.radioButton8:
                        Toast.makeText(getApplicationContext(), "Radio Button 8", Toast.LENGTH_SHORT).show();
                        break;
                    case R.id.radioButton9:
                        Toast.makeText(getApplicationContext(), "Radio Button 9", Toast.LENGTH_SHORT).show();
                        break;
                    default:
                        break;
                }
            }
        });

        buttonCheck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RadioButton radioButton;

                radioButton = findViewById(radioGroup1.getCheckedRadioButtonId());
                String selection1 = radioButton.getText() + "";

                int radioSelected2 = radioGroup2.getCheckedRadioButtonId();
                radioButton = findViewById(radioSelected2);
                String selection2 = radioButton.getText() + "";

                int radioSelected3 = radioGroup3.getCheckedRadioButtonId();
                radioButton = findViewById(radioSelected3);
                String selection3;

                if (radioButton!= null) {
                    selection3 = radioButton.getText() + "";

                } else {
                    selection3 = "     ×";
                }

                Toast.makeText(MainActivity.this,
                           "CHECKED\n" +
                                selection1 + "\n" +
                                selection2 + "\n" +
                                selection3
                                ,
                        Toast.LENGTH_SHORT).show();
            }
        });

    }
}