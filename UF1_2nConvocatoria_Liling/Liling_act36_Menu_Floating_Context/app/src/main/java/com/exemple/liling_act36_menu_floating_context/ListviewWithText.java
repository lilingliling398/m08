package com.exemple.liling_act36_menu_floating_context;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

public class ListviewWithText extends AppCompatActivity {
    private ListView listView;
    ArrayList<String> cursos = new ArrayList<>();
    ArrayAdapter<String> dataAdapter;

    @Override
    public boolean onContextItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.item1:
                Toast.makeText(this, "Delete", Toast.LENGTH_SHORT).show();
                return true;
            case R.id.item2:
                Toast.makeText(this, "Share", Toast.LENGTH_SHORT).show();
                return true;
            case R.id.item3:
                Toast.makeText(this, "Web", Toast.LENGTH_SHORT).show();
                return true;
            default:
                Toast.makeText(this, "---", Toast.LENGTH_SHORT).show();
                return super.onContextItemSelected(item);
        }

    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        getMenuInflater().inflate(R.menu.my_menu, menu);
        menu.setHeaderTitle("Choose an option");
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listview_with_text);

        //hooks
        listView = findViewById(R.id.listviewText);

        cursos.add("1JSIM");
        cursos.add("2JSIM");
        cursos.add("1HISX");
        cursos.add("2HISX");
        cursos.add("1HISM");
        cursos.add("2HISM");
        cursos.add("1HIAW");
        cursos.add("2HIAW");

        dataAdapter = new ArrayAdapter<>(this, R.layout.support_simple_spinner_dropdown_item, cursos);
        listView.setAdapter(dataAdapter);
        registerForContextMenu(listView);

    }
}