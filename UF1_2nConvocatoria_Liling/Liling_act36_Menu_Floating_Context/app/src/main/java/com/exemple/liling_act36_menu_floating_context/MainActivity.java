package com.exemple.liling_act36_menu_floating_context;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {
    private Button listViewText;
    private Button gridViewText;
    private Button listViewImages;
    private Button gridViewImages;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //hooks
        listViewText = findViewById(R.id.button);
        gridViewText = findViewById(R.id.button2);
        listViewImages= findViewById(R.id.button3);
        gridViewImages= findViewById(R.id.button4);

        listViewText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, ListviewWithText.class));
            }
        });

        gridViewText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, GridviewWithText.class));
            }
        });

        listViewImages.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, ListviewImages.class));
            }
        });

        gridViewImages.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, GridviewImages.class));
            }
        });


    }
}