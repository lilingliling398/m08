package com.exemple.liling_act36_menu_floating_context;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class ListviewImages extends AppCompatActivity {
    private ListView listView;
    private Integer[] imageIDs = {
            R.drawable.amapolas,
            R.drawable.chihiro,
            R.drawable.cielo,
            R.drawable.luciernagas,
            R.drawable.mononoke,
            R.drawable.nicky,
            R.drawable.porco,
            R.drawable.totoro,
            R.drawable.viento,
    };
    String[] imageText = {"amapolas", "chihiro", "cielo", "luciernagas", "mononoke", "nicky", "porco", "totoro", "viento"};
    private CustomAdapter adapter;

    @Override
    public boolean onContextItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.item1:
                Toast.makeText(this, "Delete", Toast.LENGTH_SHORT).show();
                return true;
            case R.id.item2:
                Toast.makeText(this, "Share", Toast.LENGTH_SHORT).show();
                return true;
            case R.id.item3:
                Toast.makeText(this, "Web", Toast.LENGTH_SHORT).show();
                return true;
            default:
                Toast.makeText(this, "---", Toast.LENGTH_SHORT).show();
                return super.onContextItemSelected(item);
        }

    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        getMenuInflater().inflate(R.menu.my_menu, menu);
        menu.setHeaderTitle("Choose an option");
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listview_images);

        //hooks
        listView = findViewById(R.id.listviewImages);

        adapter = new CustomAdapter(imageIDs, imageText);
        listView.setAdapter(adapter);
        registerForContextMenu(listView);
    }

    private class CustomAdapter extends BaseAdapter {
        private Integer[] imagesID;
        private String[] imageText;

        public CustomAdapter(Integer[] imagesID, String[] imageText) {
            this.imagesID = imagesID;
            this.imageText = imageText;
        }

        @Override
        public int getCount() {
            return imagesID.length;
        }

        @Override
        public Object getItem(int position) {
            return imagesID[position];
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View view = getLayoutInflater().inflate(R.layout.my_row, null);
            TextView textRow = view.findViewById(R.id.textRow);
            ImageView imageRow = view.findViewById(R.id.imageRow);
            textRow.setText(imageText[position]);
            imageRow.setImageResource(imagesID[position]);
            return view;
        }
    }
}

