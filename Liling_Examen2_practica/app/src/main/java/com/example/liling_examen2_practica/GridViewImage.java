package com.example.liling_examen2_practica;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class GridViewImage extends AppCompatActivity {
    int[] imagenes =  {R.drawable.amapolas, R.drawable.arrietty, R.drawable.chihiro, R.drawable.marnie, R.drawable.ponyo, R.drawable.prncesakaguya, R.drawable.totoro, R.drawable.mononoke, R.drawable.ghibli};
    GridView gridViewImager;

    List<String> stringList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_grid_view_image);

        //hook
        gridViewImager = findViewById(R.id.gridViewImage);

        stringList.add("Amapolas");
        stringList.add("Arrietty");
        stringList.add("Chihiro");
        stringList.add("Marnnie");
        stringList.add("Ponyo");
        stringList.add("Princesa Sakaguya");
        stringList.add("Totoro");
        stringList.add("mononoke");
        stringList.add("Ghibli");

        //clic derecho create inner class
        CustomAdapter customAdapter = new CustomAdapter();

        //pasarle el custom adapter(no array adapter)
        gridViewImager.setAdapter(customAdapter);


        registerForContextMenu(gridViewImager);

    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        menu.setHeaderTitle("Escoge una opcion");
        menu.add(0, 1, 1, "Eliminar");
        menu.add(0, 2, 2, "Compartir");
        menu.add(0, 3, 3, "Web");
    }

    //ctrl + o
    @Override
    public boolean onContextItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case 1:
                Toast.makeText(this, "Eliminar", Toast.LENGTH_SHORT).show();
                return true;
            case 2:
                Toast.makeText(this, "Compartir", Toast.LENGTH_SHORT).show();
                return true;
            case 3:
                Toast.makeText(this, "Web", Toast.LENGTH_SHORT).show();
                return true;
            default:
                return super.onContextItemSelected(item);
        }
    }


    private class CustomAdapter extends BaseAdapter {
        @Override
        public int getCount() {
            return imagenes.length;
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        //!!!!!!!
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View view = getLayoutInflater().inflate(R.layout.list_imagen2, null);
            //se pasan los parametros de list_imagen.xml
            TextView textView = view.findViewById(R.id.textGridView);
            ImageView imageView = view.findViewById(R.id.imageGridView);

            textView.setText(stringList.get(position));
            imageView.setImageResource(imagenes[position]);
            return view;
        }
    }
}