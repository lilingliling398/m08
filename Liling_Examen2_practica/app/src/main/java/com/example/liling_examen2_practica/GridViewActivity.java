package com.example.liling_examen2_practica;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class GridViewActivity extends AppCompatActivity {
    List<String> cursos = new ArrayList<>();
    ArrayAdapter<String> arrayAdapter;
    GridView gridView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_grid_view);

        //hook
        gridView = findViewById(R.id.gridView);

        cursos.add("1JSIM");
        cursos.add("2JSIM");
        cursos.add("1HISX");
        cursos.add("2HISX");
        cursos.add("1HISM");
        cursos.add("2HISM");
        cursos.add("1HIAW");
        cursos.add("2HIAW");
        cursos.add("1JISM");
        cursos.add("2JISM");
        cursos.add("1HISX");
        cursos.add("2HISX");
        cursos.add("1WIAM");
        cursos.add("2WIAM");
        cursos.add("1WIAW");
        cursos.add("2WIAW");

        arrayAdapter = new ArrayAdapter<>(this, R.layout.support_simple_spinner_dropdown_item,cursos);

        gridView.setAdapter(arrayAdapter);

        registerForContextMenu(gridView);

    }

    //ctrl + o
    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        menu.setHeaderTitle("Escoge una opcion");
        menu.add(0, 1, 1, "Eliminar");
        menu.add(0, 2, 2, "Compartir");
        menu.add(0, 3, 3, "Web");
    }

    //ctrl + o
    @Override
    public boolean onContextItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case 1:
                Toast.makeText(this, "Eliminar", Toast.LENGTH_SHORT).show();
                return true;
            case 2:
                Toast.makeText(this, "Compartir", Toast.LENGTH_SHORT).show();
                return true;
            case 3:
                Toast.makeText(this, "Web", Toast.LENGTH_SHORT).show();
                return true;
            default:
                return super.onContextItemSelected(item);
        }
    }
}