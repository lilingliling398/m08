package com.example.liling_act3_viewpagerslider;

import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.SeekBar;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {
    MediaPlayer mediaPlayer;
    Button btPlay;
    Button btStop;
    Button btRewind;
    Button btFastForward;
    SeekBar seekBarVolume;
    SeekBar seekBarProgress;

    @Override
    protected void onStop() {
        super.onStop();

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        //hook
        btPlay = findViewById(R.id.buttonPlay);
        btStop = findViewById(R.id.buttonPause);

        mediaPlayer = MediaPlayer.create(this, R.raw.willow);
        mediaPlayer.start();

        btPlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playAudio();
            }
        });
        btStop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pauseAudio();
            }
        });


    }

    private void releaseMediaPlayer() {
        if (mediaPlayer != null) {
            mediaPlayer.release();
            mediaPlayer = null;
        }
    }

    public void playAudio() {
        mediaPlayer.start();
    }

    public void pauseAudio() {
        mediaPlayer.pause();
    }


}