package com.example.liling_act2d_recyclerview;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    ArrayList<Picture> pictures = new ArrayList<>();
    RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //hook
        recyclerView = findViewById(R.id.recyclerView);
        initData();

        //Crear l'objecte MyAdapter
        MyAdapter myAdapter = new MyAdapter(pictures, this);
        recyclerView.setAdapter(myAdapter);
        recyclerView.setLayoutManager(new GridLayoutManager(this, 3,
                RecyclerView.VERTICAL, false));

    }

    private void initData() {
        String zero = "0";
        for (int i = 1; i <= 38; i++) {
            if (i > 9) {
                zero = "";
            }
            String titulo = "Pic" + zero + i;
            String desc = "Descripcion" + zero + i;
            String imagenURL = "https://joanseculi.com/images/img" + zero + i + ".jpg";
            Picture picture = new Picture(titulo, desc, imagenURL);

            pictures.add(picture);
        }
    }
}