package com.example.liling_act2d_recyclerview;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.TextureView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyViewHolder> {
    private ArrayList<Picture> pictures;
    private Context context;

    public MyAdapter(ArrayList<Picture> pictures, Context context) {
        this.pictures = pictures;
        this.context = context;
    }
    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);

        View view = inflater.inflate(R.layout.my_row, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        Picasso.get().load(pictures.get(position).getImagen()).fit().centerCrop().into(holder.imageUrl);
        holder.titulo.setText(pictures.get(position).getTitulo());

        holder.constraintLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, DetailActivity.class);
                intent.putExtra("titulo", pictures.get(position).getTitulo());
                intent.putExtra("desc", pictures.get(position).getDescripcion());
                intent.putExtra("imagenURL", pictures.get(position).getImagen());

                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return pictures.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView titulo;
        private ImageView imageUrl;

        ConstraintLayout constraintLayout;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            titulo = itemView.findViewById(R.id.textTitulo);
            imageUrl = itemView.findViewById(R.id.imageURLView);

            constraintLayout = itemView.findViewById(R.id.rowLayout);
        }
    }


}
