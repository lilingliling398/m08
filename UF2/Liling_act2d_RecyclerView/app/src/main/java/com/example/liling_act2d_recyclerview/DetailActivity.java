package com.example.liling_act2d_recyclerview;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

public class DetailActivity extends AppCompatActivity {
    private TextView tituloView;
    private TextView descView;
    private ImageView imagenView;
    String titulo, desc, imagen;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        //hook
        tituloView = findViewById(R.id.detailTitulo);
        descView = findViewById(R.id.detailDesc);
        imagenView = findViewById(R.id.detailURL);


        getData();
        setData();

    }

    private void getData() {
        if (getIntent().hasExtra("titulo") && getIntent().hasExtra("desc")) {
            titulo = getIntent().getStringExtra("titulo");
            desc = getIntent().getStringExtra("desc");
            imagen = getIntent().getStringExtra("imagenURL");
        } else {
            Toast.makeText(this, "No data found", Toast.LENGTH_SHORT).show();
        }
    }

    private void setData() {
        tituloView.setText(titulo);
        descView.setText(desc);
        Picasso.get().load(imagen)
                .into(imagenView);
    }


}