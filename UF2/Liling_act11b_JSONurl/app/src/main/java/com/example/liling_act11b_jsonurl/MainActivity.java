package com.example.liling_act11b_jsonurl;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    ArrayList<Peak> peaks = new ArrayList<>();
    RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //hook
        recyclerView = findViewById(R.id.recyclerView);

        //Layout Instanciar MyAdapter i assignar-lo al RecyclerView
        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        MyAdapter myAdapter = new MyAdapter(peaks, MainActivity.this);
        recyclerView.setAdapter(myAdapter);

        try {
            //Getting JSONObject from file
            JSONObject obj = new JSONObject(loadJSONObjectefromAssets());
            //obtenir el JSONArray aqui
            JSONArray peakArray = obj.getJSONArray("peaks");
            for (int i = 0; i < peakArray.length(); i++) {
                JSONObject peakDetail = peakArray.getJSONObject(i);
                //afegir les dades a cada un dels ArrayList
                String nombre = peakDetail.getString("name");
                String altura = peakDetail.getString("height");
                String prominencia = peakDetail.getString("prominence");
                String zona = peakDetail.getString("zone");
                String imagen = peakDetail.getString("url");
                String pais = peakDetail.getString("country");
                peaks.add(new Peak(nombre, altura, pais,prominencia, zona, imagen));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private String loadJSONObjectefromAssets() {
        String json = null;
        try {
            InputStream input = getAssets().open("peaks.json");
            //Returns an estimate of the number of bytes that can be read
            int size = input.available();
            byte[] buffer = new byte[size];
            //Reads some number of bytes from the input stream
            //and stores them into the buffer array.
            input.read(buffer);
            input.close();
            json = new String(buffer, "UTF-8"); //accpet all char types
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
        return json;
    }
}