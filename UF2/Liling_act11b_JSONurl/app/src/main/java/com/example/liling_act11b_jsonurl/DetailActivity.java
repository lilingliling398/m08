package com.example.liling_act11b_jsonurl;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

public class DetailActivity extends AppCompatActivity {
    private TextView altura;
    private TextView nombre;
    private TextView prominencia;
    private TextView zona;
    private TextView pais;
    private ImageView imageView;
    String sNombre, sAltura, sProm, sZona, sPais, sImagen;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        //hook
        nombre = findViewById(R.id.nombreT);
        altura = findViewById(R.id.alturaT);
        prominencia = findViewById(R.id.promiT);
        zona = findViewById(R.id.zonaT);
        pais = findViewById(R.id.paisT);
        imageView = findViewById(R.id.imageView);

        getData();
        setData();
    }

    private void getData() {
        if (getIntent().hasExtra("nombre") && getIntent().hasExtra("altura")) {
            sNombre = getIntent().getStringExtra("nombre");
            sAltura = getIntent().getStringExtra("altura");
            sProm = getIntent().getStringExtra("prominencia");
            sZona = getIntent().getStringExtra("zona");
            sPais = getIntent().getStringExtra("pais");
            sImagen = getIntent().getStringExtra("imagen");
        } else {
            Toast.makeText(this, "No data found", Toast.LENGTH_SHORT).show();
        }
    }

    private void setData() {
        altura.setText(sNombre);
        nombre.setText(sAltura);
        prominencia.setText(sProm);
        zona.setText(sZona);
        pais.setText(sPais);
        Picasso.get().load(sImagen).into(imageView);
    }
}