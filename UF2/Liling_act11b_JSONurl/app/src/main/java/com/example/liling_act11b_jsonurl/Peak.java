package com.example.liling_act11b_jsonurl;

import android.widget.ImageView;
import android.widget.TextView;

public class Peak {
    private String nombre;
    private String altura;
    private String pais;
    private String prominencia;
    private String zona;
    private String imagenUrl;

    public Peak(String nombre, String altura, String pais, String prominencia, String zona, String imagenUrl) {
        this.nombre = nombre;
        this.altura = altura;
        this.pais = pais;
        this.prominencia = prominencia;
        this.zona = zona;
        this.imagenUrl = imagenUrl;
    }

    public String getZona() {
        return zona;
    }

    public void setZona(String zona) {
        this.zona = zona;
    }

    public String getProminencia() {
        return prominencia;
    }

    public void setProminencia(String prominencia) {
        this.prominencia = prominencia;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getAltura() {
        return altura;
    }

    public void setAltura(String altura) {
        this.altura = altura;
    }

    public String getPais() {
        return pais;
    }

    public void setPais(String pais) {
        this.pais = pais;
    }

    public String getImagenUrl() {
        return imagenUrl;
    }

    public void setImagenUrl(String imagenUrl) {
        this.imagenUrl = imagenUrl;
    }
}
