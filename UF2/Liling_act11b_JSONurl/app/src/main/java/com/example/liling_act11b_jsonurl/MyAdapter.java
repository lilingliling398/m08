package com.example.liling_act11b_jsonurl;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyViewHolder> {
    ArrayList<Peak> peaks = new ArrayList<>();
    Context mContext;

    public MyAdapter(ArrayList<Peak> peaks, Context mContext) {
        this.peaks = peaks;
        this.mContext = mContext;
    }

    @NonNull
    @Override
    public MyAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(mContext);
        View view = layoutInflater.inflate(R.layout.peak_layout, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyAdapter.MyViewHolder holder, int position) {
        holder.nombre.setText(peaks.get(position).getNombre());
        holder.altura.setText(peaks.get(position).getAltura());
        holder.pais.setText(peaks.get(position).getPais());

        Picasso.get().load(peaks.get(position).getImagenUrl())
                .fit()
                .centerCrop()
                .into(holder.imagenUrl);


        holder.rowLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, DetailActivity.class);
                intent.putExtra("nombre", peaks.get(position).getNombre());
                intent.putExtra("altura", peaks.get(position).getAltura());
                intent.putExtra("prominencia", peaks.get(position).getProminencia());
                intent.putExtra("zona", peaks.get(position).getZona());
                intent.putExtra("pais", peaks.get(position).getPais());
                intent.putExtra("imagen", peaks.get(position).getImagenUrl());
                mContext.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return peaks.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView nombre;
        TextView altura;
        TextView pais;
        ImageView imagenUrl;

        ConstraintLayout rowLayout;


        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            nombre = itemView.findViewById(R.id.nombreText);
            altura = itemView.findViewById(R.id.alturaText);
            pais = itemView.findViewById(R.id.paisText);
            imagenUrl = itemView.findViewById(R.id.imagenURL);

            rowLayout = itemView.findViewById(R.id.rowLayout);
        }
    }
}
