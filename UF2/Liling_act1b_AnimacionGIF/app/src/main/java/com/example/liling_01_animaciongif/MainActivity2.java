package com.example.liling_01_animaciongif;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ListActivity;
import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.View;
import android.widget.ImageView;

public class MainActivity2 extends AppCompatActivity {
    ImageView imageView;
    AnimationDrawable animation;
    int SPLASH_SCREEN = 1200;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        //hook
        imageView = findViewById(R.id.imageView2);

        //opcion2 iniciarlo con un action e wdx
        animation = new AnimationDrawable();
        animation.addFrame(getResources().getDrawable(R.drawable.rocket17), 40);
        animation.addFrame(getResources().getDrawable(R.drawable.rocket18), 40);
        animation.addFrame(getResources().getDrawable(R.drawable.rocket19), 40);
        animation.addFrame(getResources().getDrawable(R.drawable.rocket20), 40);
        animation.addFrame(getResources().getDrawable(R.drawable.rocket21), 40);
        animation.addFrame(getResources().getDrawable(R.drawable.rocket22), 40);
        animation.addFrame(getResources().getDrawable(R.drawable.rocket23), 40);
        animation.addFrame(getResources().getDrawable(R.drawable.rocket24), 40);
        animation.addFrame(getResources().getDrawable(R.drawable.rocket25), 40);
        animation.addFrame(getResources().getDrawable(R.drawable.rocket26), 40);
        animation.addFrame(getResources().getDrawable(R.drawable.rocket27), 40);
        animation.addFrame(getResources().getDrawable(R.drawable.rocket28), 40);
        animation.addFrame(getResources().getDrawable(R.drawable.rocket29), 40);
        animation.addFrame(getResources().getDrawable(R.drawable.rocket30), 40);

        String sImage;

        for (int i = 30; i <= 167; i++) {
            sImage = "rocket";
            animation.addFrame(
                    getResources().getDrawable(getResources().getIdentifier(sImage + i, "drawable", getPackageName())), 20);
        }
        for (int i = 1; i <= 225; i++) {
            sImage = "spaceship";
            animation.addFrame(
                    getResources().getDrawable(getResources().getIdentifier(sImage + i, "drawable", getPackageName())), 20);
        }
        animation.setOneShot(false);

        imageView.setImageDrawable(animation);
        animation.start();


       /* imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Intent intent = new Intent(MainActivity2.this, ListActivity.class);
                        startActivity(intent);
                    }
                }, SPLASH_SCREEN);
            }
        });*/


       /* String sImage;
        for (int i = 1; i <= 151; i++) {
            sImage = "rocket";
            animation.addFrame(
                    getResources().getDrawable(getResources().getIdentifier(sImage + i, "drawable", getPackageName())), 20);
        }*/
    }
}