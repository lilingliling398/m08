package com.example.liling_examen_m08_uf2;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import java.util.ArrayList;

public class ViewPagerActivity extends AppCompatActivity {
    ArrayList<String> imagesURL = new ArrayList<>();
    private TextView textRight;
    private TextView textLeft;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_pager);

        //Hook
        ViewPager viewPager = (ViewPager) findViewById(R.id.viewPager);
        textRight = findViewById(R.id.derecha);
        textLeft = findViewById(R.id.izquierda);

        textRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewPager.arrowScroll(View.FOCUS_RIGHT);
            }
        });
        textLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public
            void onClick(View v) {
                viewPager.arrowScroll(View.FOCUS_LEFT);
            }
        });

    }
}