package com.example.liling_examen_m08_uf2;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    public static final String PAIS = "com.example.liling_examen_m08_uf2.PAIS";
    String[] paises = {"Select a country", "Spain", "England", "France", "Italy", "Jamaica", "Mexico"};
    private Spinner spinnerPerJava;
    String paisSelect;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        //hook
        spinnerPerJava = findViewById(R.id.spinnerPaises);


        ArrayAdapter<String> dataAdapter = new ArrayAdapter<>(this, R.layout.support_simple_spinner_dropdown_item, paises);
        spinnerPerJava.setAdapter(dataAdapter);


        Intent intent = new Intent(this, DetailActivity.class);


        spinnerPerJava.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String selectedCondition = parent.getItemAtPosition(position).toString();
                Toast.makeText(MainActivity.this, selectedCondition, Toast.LENGTH_SHORT).show();

                paisSelect = paises[position];
                if (!paisSelect.equals("Select a country")) {
                    intent.putExtra(PAIS, paisSelect);
                    startActivity(intent);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                Toast.makeText(MainActivity.this, "Nothing Selected", Toast.LENGTH_SHORT).show();
            }


        });
    }
}

