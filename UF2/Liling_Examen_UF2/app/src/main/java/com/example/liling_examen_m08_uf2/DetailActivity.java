package com.example.liling_examen_m08_uf2;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.content.UriMatcher;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class DetailActivity extends AppCompatActivity {
    ArrayList<Country> countries = new ArrayList<>();
    private RecyclerView recyclerView;
    private static String JSON_URL = "";
    MyAdapter myAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        //hook
        recyclerView = findViewById(R.id.recyclerView);


        Intent intent = getIntent();
        String paisSeleccionado = intent.getStringExtra(MainActivity.PAIS);
        System.out.println(paisSeleccionado);

        JSON_URL = "https://www.thesportsdb.com/api/v1/json/1/search_all_leagues.php?c=" + paisSeleccionado + "&s=Soccer";

        getCountries(JSON_URL);


    }

    private void getCountries(String URL){
        RequestQueue queue = Volley.newRequestQueue(this);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, URL, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    JSONArray jsonArray = response.getJSONArray("countrys");
                    for (int i = 0; i < jsonArray.length(); i++) {
                        Country country = new Country();
                        JSONObject countryObject = jsonArray.getJSONObject(i);

                        country.setUrlImage(countryObject.getString("strBadge"));
                        country.setTitle(countryObject.getString("strLeague"));
                        country.setDescription(countryObject.getString("strDescriptionEN"));
                        country.setVisitWeb(countryObject.getString("strWebsite"));

                        ArrayList<String> imagesArray = new ArrayList<>();
                        for (int j = 1; j <= 4; j++) {
                            if (countryObject.getString("strFanart" + j) != null) {
                                imagesArray.add(countryObject.getString("strFanart" + j));
                            }
                        }
                        country.setImages(imagesArray);

                        countries.add(country);
                    }



                } catch (JSONException e) {
                    e.printStackTrace();
                }
                recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
                myAdapter = new MyAdapter(getApplicationContext(), countries);
                recyclerView.setAdapter(myAdapter);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("tag", "onErrorResponse: " + error.getMessage());
            }
        });

        queue.add(jsonObjectRequest);
    }

    /*private void getCountries( ) {
        RequestQueue queue = Volley.newRequestQueue(this);
        JSONObject jsonArrayRequest = new JSONObject(
                Request.Method.GET,
                ENGLAND_JSON_URL,
                null,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        for (int i = 0; i < response.length(); i++) {
                            try {
                                JsonArrayRequest countryObject = response.getJSONObject(i);
                                Country country = new Country();
                                country.setUrlImage(countryObject.getString("strBadge"));
                                country.setTitle(countryObject.getString("strLeague"));
                                country.setDescription(countryObject.getString("strDescriptionEN"));
                                country.setVisitWeb(countryObject.getString("strWebsite"));

                                ArrayList<String> imagesArray = new ArrayList<>();
                                for (int j = 1; j <= 4; j++) {
                                    if (countryObject.getString("strFanart" + j) != null) {
                                        imagesArray.add(countryObject.getString("strFanart" + j));
                                    }
                                }
                                country.setImages(imagesArray);
                                countries.add(country);

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
                        myAdapter = new MyAdapter(getApplicationContext(), countries);
                        recyclerView.setAdapter(myAdapter);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("tag", "onErrorResponse: " + error.getMessage());
                    }
                }
        );
        queue.add(jsonArrayRequest);
    }*/
}