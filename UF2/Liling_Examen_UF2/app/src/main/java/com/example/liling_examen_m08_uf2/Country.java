package com.example.liling_examen_m08_uf2;

import android.widget.Button;
import android.widget.ImageView;

import java.util.ArrayList;

public class Country {
    private String nameCountry;
    private String urlImage;
    private String title;
    private String description;
    private String visitWeb;
    private ArrayList<String> images;


    public Country(){

    }

    public Country(String nameCountry, String urlImage, String title, String description, String visitWeb, ArrayList<String> images) {
        this.nameCountry = nameCountry;
        this.urlImage = urlImage;
        this.title = title;
        this.description = description;
        this.visitWeb = visitWeb;
        this.images = images;
    }

    public String getNameCountry() {
        return nameCountry;
    }

    public void setNameCountry(String nameCountry) {
        this.nameCountry = nameCountry;
    }

    public String getUrlImage() {
        return urlImage;
    }

    public void setUrlImage(String urlImage) {
        this.urlImage = urlImage;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getVisitWeb() {
        return visitWeb;
    }

    public void setVisitWeb(String visitWeb) {
        this.visitWeb = visitWeb;
    }

    public ArrayList<String> getImages() {
        return images;
    }

    public void setImages(ArrayList<String> images) {
        this.images = images;
    }

    //Recull els valors de l'array meals i els presenta com un String //separat per comes
    public String getImagesCountrie() {
        String value = "";
        for (int i = 0; i < images.size(); i++) {
            if (i == 0) {
                value = images.get(i);
            } else {
                value = value + ", " + images.get(i);
            }
        }
        return value;
    }
}
