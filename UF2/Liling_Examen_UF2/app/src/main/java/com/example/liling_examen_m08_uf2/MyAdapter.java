package com.example.liling_examen_m08_uf2;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.util.List;


public class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyViewHolder> {
    private Context mContext;
    private List<Country> mCountries;

    public MyAdapter(Context mContext, List<Country> mCountries) {
        this.mContext = mContext;
        this.mCountries = mCountries;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(mContext);
        View view = layoutInflater.inflate(R.layout.country_layout, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public int getItemCount() {
        return mCountries.size();
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        holder.title.setText(mCountries.get(position).getTitle());
        holder.description.setText(mCountries.get(position).getDescription());


        holder.visitWebBT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String url= mCountries.get(position).getVisitWeb();
                Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                //startActivity(i);
            }
        });



        if (mCountries.get(position).getImages().size()== 0 || mCountries.get(position).getImages()==null){
            holder.imagesBT.setVisibility(View.INVISIBLE);
        }else {

            holder.imagesBT.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                }
            });
        }




        Picasso.get().load(mCountries.get(position).getUrlImage()).into(holder.urlImage);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView urlImage;
        TextView title;
        TextView description;
        Button visitWebBT;
        Button imagesBT;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            urlImage = itemView.findViewById(R.id.imageCountry);
            title = itemView.findViewById(R.id.textTitle);
            description = itemView.findViewById(R.id.textDesc);
            visitWebBT = itemView.findViewById(R.id.webBT);
            imagesBT = itemView.findViewById(R.id.imagesBT);
        }
    }
}

