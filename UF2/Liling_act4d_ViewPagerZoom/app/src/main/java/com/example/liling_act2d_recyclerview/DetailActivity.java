package com.example.liling_act2d_recyclerview;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.util.ArrayList;

public class DetailActivity extends AppCompatActivity {
    private TextView tituloView;
    private TextView descView;
    private ViewPager viewPager;
    String titulo, desc, imagen;

    ArrayList<String> urls = new ArrayList<>();

    private TextView textRight;
    private TextView textLeft;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        //hook
        tituloView = findViewById(R.id.detailTitulo);
        descView = findViewById(R.id.detailDesc);
        viewPager = findViewById(R.id.viewPager);

        textRight = findViewById(R.id.textRight);
        textLeft = findViewById(R.id.textLeft);

        initImagem();

        getData();
        setData();

        DetailAdapter adapter = new DetailAdapter(urls, this);
        viewPager.setAdapter(adapter);

        textRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewPager.arrowScroll(View.FOCUS_RIGHT);
            }
        });

        textLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewPager.arrowScroll(View.FOCUS_LEFT);
            }
        });
    }

    private void initImagem() {
        String zero = "0";
        for (int i = 1; i <= 38; i++) {
            if (i > 9) {
                zero = "";
            }
            String imagenURL = "https://joanseculi.com/images/img" + zero + i + ".jpg";
            urls.add(imagenURL);
        }
    }


    private void getData() {
        if (getIntent().hasExtra("titulo") && getIntent().hasExtra("desc")) {
            titulo = getIntent().getStringExtra("titulo");
            desc = getIntent().getStringExtra("desc");
            imagen = getIntent().getStringExtra("imagenURL");
        } else {
            Toast.makeText(this, "No data found", Toast.LENGTH_SHORT).show();
        }
    }

    private void setData() {
        tituloView.setText(titulo);
        descView.setText(desc);

    }


}