package com.example.liling_act2d_recyclerview;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class DetailAdapter extends PagerAdapter {
    private Context mContext;
    private ArrayList<String> mImagesIds;


    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        ImageView imageView = new ImageView(mContext);
        Picasso.get()
                .load(mImagesIds.get(position))
                .fit()
                .centerInside()
                .into(imageView);
        container.addView(imageView);
        return imageView;
    }

    public DetailAdapter( ArrayList<String> mImagesIds, Context mContext) {
        this.mContext = mContext;
        this.mImagesIds = mImagesIds;
    }

    @Override
    public int getCount() {
        return mImagesIds.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return false;
    }
}
