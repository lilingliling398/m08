package com.example.liling_act2d_recyclerview;

import java.util.ArrayList;

public class Picture {
    private String titulo;
    private String descripcion;
    private String imagen;
    private ArrayList<String> urlImages;

    public Picture(String titulo, String descripcion, String imagen) {
        this.titulo = titulo;
        this.descripcion = descripcion;
        this.imagen = imagen;
    }

    public Picture(String titulo, String descripcion, String imagen, ArrayList<String> urlImages) {
        this.titulo = titulo;
        this.descripcion = descripcion;
        this.imagen = imagen;
        this.urlImages = urlImages;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getImagen() {
        return imagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }

    public ArrayList<String> getUrlImages() {
        return urlImages;
    }

    public void setUrlImages(ArrayList<String> urlImages) {
        this.urlImages = urlImages;
    }
}
