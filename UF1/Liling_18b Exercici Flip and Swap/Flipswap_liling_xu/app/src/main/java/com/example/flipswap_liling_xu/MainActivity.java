package com.example.flipswap_liling_xu;

import androidx.appcompat.app.AppCompatActivity;

import android.animation.Animator;
import android.animation.AnimatorInflater;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

public class MainActivity extends AppCompatActivity {

    private ImageView imageView1;
    private ImageView imageView2;
    private ImageView imageView3;
    private ImageView imageView4;
    private ImageView imageView5;

    private ImageView imageView15;
    private ImageView imageView25;
    private ImageView imageView35;
    private ImageView imageView45;

    private RelativeLayout relativeLayout1;

    private boolean girarDerecha1 = true;
    private boolean girarDerecha2 = true;
    private boolean girarDerecha3 = true;
    private boolean girarDerecha4 = true;
    private boolean girarDerecha5 = true;

    ObjectAnimator objectAnimator1;
    ObjectAnimator objectAnimator2;
    ObjectAnimator objectAnimator3;
    ObjectAnimator objectAnimator4;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //hook
        imageView1 = (ImageView) findViewById(R.id.imageView1);
        imageView2 = (ImageView) findViewById(R.id.imageView2);
        imageView3 = (ImageView) findViewById(R.id.imageView3);
        imageView4 = (ImageView) findViewById(R.id.imageView4);
        imageView5 = (ImageView) findViewById(R.id.imageView5);

        imageView15 = (ImageView) findViewById(R.id.imageView15);
        imageView25 = (ImageView) findViewById(R.id.imageView25);
        imageView35 = (ImageView) findViewById(R.id.imageView35);
        imageView45 = (ImageView) findViewById(R.id.imageView45);

        relativeLayout1 = (RelativeLayout) findViewById(R.id.RelativeLayout1);


        imageView1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (girarDerecha1) {
                    girarIzquierda(v, imageView1, imageView15, "RotationY");
                    girarDerecha1 = false;
                } else {
                    girarDerecha(v, imageView15, imageView1, "RotationY");
                    girarDerecha1 = true;
                }
            }
        });

        imageView2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (girarDerecha2) {
                    girarIzquierda(v, imageView2, imageView25, "RotationX");
                    girarDerecha2 = false;
                } else {
                    girarDerecha(v, imageView25, imageView2, "RotationX");
                    girarDerecha2 = true;
                }
            }
        });

        imageView3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (girarDerecha3) {
                    girarIzquierda(v, imageView3, imageView35, "RotationY");
                    girarDerecha3 = false;
                } else {
                    girarDerecha(v, imageView35, imageView3, "RotationY");
                    girarDerecha3 = true;
                }
            }
        });

        imageView4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (girarDerecha4) {
                    girarIzquierda(v, imageView4, imageView45, "RotationY");
                    girarDerecha4 = false;
                } else {
                    girarDerecha(v, imageView45, imageView4, "RotationY");
                    girarDerecha4 = true;
                }
            }
        });

        imageView5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (girarDerecha5) {
                    objectAnimator1 = ObjectAnimator.ofFloat(imageView5, "RotationY", 0f, 180f);
                    objectAnimator2 = ObjectAnimator.ofFloat(imageView5, "alpha", 1f, 0.3f);
                    objectAnimator3 = ObjectAnimator.ofFloat(relativeLayout1, "RotationY", 180f, 360f);
                    objectAnimator4 = ObjectAnimator.ofFloat(relativeLayout1, "alpha", 0f, 1f);

                    objectAnimator1.setDuration(500);
                    objectAnimator2.setStartDelay(250);
                    objectAnimator3.setDuration(500);
                    objectAnimator4.setStartDelay(250);

                    AnimatorSet animatorSet = new AnimatorSet();

                    animatorSet.playTogether(objectAnimator1, objectAnimator2, objectAnimator3, objectAnimator4);
                    animatorSet.start();

                    girarDerecha5 = false;

                } else {
                    objectAnimator1 = ObjectAnimator.ofFloat(relativeLayout1, "RotationY", 360f, 180f);
                    objectAnimator2 = ObjectAnimator.ofFloat(relativeLayout1, "alpha", 1f, 0f);
                    objectAnimator3 = ObjectAnimator.ofFloat(imageView5, "RotationY", 180f, 0f);
                    objectAnimator4 = ObjectAnimator.ofFloat(imageView5, "alpha", 0.3f, 1f);

                    objectAnimator1.setDuration(500);
                    objectAnimator2.setStartDelay(250);
                    objectAnimator3.setDuration(500);
                    objectAnimator4.setStartDelay(250);

                    AnimatorSet animatorSet = new AnimatorSet();

                    animatorSet.playTogether(objectAnimator1, objectAnimator2, objectAnimator3, objectAnimator4);
                    animatorSet.start();

                    girarDerecha5 = true;
                }

            }
        });
    }

    public void girarIzquierda(View v, ImageView imageView, ImageView imageView1, String rotacion) {
        objectAnimator1 = ObjectAnimator.ofFloat(imageView, rotacion, 0f, 180f);
        objectAnimator2 = ObjectAnimator.ofFloat(imageView, "alpha", 1f, 0f);
        objectAnimator3 = ObjectAnimator.ofFloat(imageView1, rotacion, 180f, 360f);
        objectAnimator4 = ObjectAnimator.ofFloat(imageView1, "alpha", 0f, 1f);

        objectAnimator1.setDuration(500);
        objectAnimator2.setStartDelay(250);
        objectAnimator3.setDuration(500);
        objectAnimator4.setStartDelay(250);

        AnimatorSet animatorSet = new AnimatorSet();

        animatorSet.playTogether(objectAnimator1, objectAnimator2, objectAnimator3, objectAnimator4);
        animatorSet.start();
    }

    public void girarDerecha(View v, ImageView imageView1, ImageView imageView, String rotacion) {
        objectAnimator1 = ObjectAnimator.ofFloat(imageView, rotacion, 180f, 0f);
        objectAnimator2 = ObjectAnimator.ofFloat(imageView, "alpha", 0f, 1f);
        objectAnimator3 = ObjectAnimator.ofFloat(imageView1, rotacion, 360f, 180f);
        objectAnimator4 = ObjectAnimator.ofFloat(imageView1, "alpha", 1f, 0f);

        objectAnimator1.setDuration(500);
        objectAnimator2.setStartDelay(250);
        objectAnimator3.setDuration(500);
        objectAnimator4.setStartDelay(250);

        AnimatorSet animatorSet = new AnimatorSet();

        animatorSet.playTogether(objectAnimator1, objectAnimator2, objectAnimator3, objectAnimator4);
        animatorSet.start();

    }

}