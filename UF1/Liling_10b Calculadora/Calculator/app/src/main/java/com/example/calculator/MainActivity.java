package com.example.calculator;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    private TextView textResult;
    private EditText num1;
    private EditText num2;
    private Button buttonAdd;
    private Button buttonSubst;
    private Button buttonMult;
    private Button buttonDiv;
    private Button buttonPower;
    private Button buttonCe;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //hook=assinacion

        textResult = (TextView) findViewById(R.id.textView);
        num1 = (EditText) findViewById(R.id.editTextNumberDecimal);
        num2 = (EditText) findViewById(R.id.editTextNumberDecimal2);
        buttonAdd = (Button) findViewById(R.id.buttonAdd);
        buttonSubst = (Button) findViewById(R.id.buttonSubst);
        buttonMult = (Button) findViewById(R.id.buttonMult);
        buttonPower = (Button) findViewById(R.id.buttonPower);
        buttonCe = (Button) findViewById(R.id.buttonCe);

        textResult.setText("0.0");

        buttonAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //suma
                if (num1.getText().toString().trim().isEmpty() || num2.getText().toString().trim().isEmpty()) {
                    Toast.makeText(MainActivity.this, "Enter a number", Toast.LENGTH_SHORT).show();
                } else {
                    double result = Double.parseDouble(num1.getText().toString().trim())
                            + Double.parseDouble(num2.getText().toString().trim());
                    textResult.setText(result+"");
                }
            }
        });

        buttonSubst.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (num1.getText().toString().trim().isEmpty() || num2.getText().toString().trim().isEmpty()) {
                    Toast.makeText(MainActivity.this, "Enter a number", Toast.LENGTH_SHORT).show();
                } else {
                    double result = Double.parseDouble(num1.getText().toString().trim())
                            - Double.parseDouble(num2.getText().toString().trim());
                    textResult.setText(result+"");
                }
            }
        });

        buttonMult.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (num1.getText().toString().trim().isEmpty() || num2.getText().toString().trim().isEmpty()) {
                    Toast.makeText(MainActivity.this, "Enter a number", Toast.LENGTH_SHORT).show();
                } else {
                    double result = Double.parseDouble(num1.getText().toString().trim())
                            * Double.parseDouble(num2.getText().toString().trim());
                    textResult.setText(result+"");
                }
            }
        });

        buttonDiv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (num1.getText().toString().trim().isEmpty() || num2.getText().toString().trim().isEmpty()) {
                    Toast.makeText(MainActivity.this, "Enter a number", Toast.LENGTH_SHORT).show();
                } else {
                    double result = Double.parseDouble(num1.getText().toString().trim())
                            / Double.parseDouble(num2.getText().toString().trim());
                    textResult.setText(result+"");
                }
            }
        });

        buttonPower.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (num1.getText().toString().trim().isEmpty() || num2.getText().toString().trim().isEmpty()) {
                    Toast.makeText(MainActivity.this, "Enter a number", Toast.LENGTH_SHORT).show();
                } else {
                    double result = Math.pow(Double.parseDouble(num1.getText().toString().trim()),
                    Double.parseDouble(num2.getText().toString().trim()) );
                    textResult.setText(result+"");
                }
            }
        });

        buttonCe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (num1.getText().toString().trim().isEmpty() || num2.getText().toString().trim().isEmpty()) {
                    Toast.makeText(MainActivity.this, "Enter a number", Toast.LENGTH_SHORT).show();
                } else {
                    textResult.setText("0.0");
                }
            }
        });

    }
}