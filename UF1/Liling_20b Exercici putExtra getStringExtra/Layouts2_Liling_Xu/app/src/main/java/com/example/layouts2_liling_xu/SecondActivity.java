package com.example.layouts2_liling_xu;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

public class SecondActivity extends AppCompatActivity {
    //declarar
    private TextView textTitle;
    private TextView subtitle;
    private ImageView imageTop;

    private ImageView productImage;
    private TextView productName;
    private TextView productDesc;
    private TextView productPrice;

    private TextView productRating;
    private TextView productFav;
    private TextView productDownl;

    private TextView productLongText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        //hook
        textTitle = (TextView) findViewById(R.id.textTitle);
        subtitle = (TextView) findViewById(R.id.textSubtitle) ;
        imageTop = (ImageView) findViewById(R.id.imageTop);

        productImage = (ImageView) findViewById(R.id.productImage);
        productName = (TextView) findViewById(R.id.productName);
        productDesc = (TextView) findViewById(R.id.productDesc);
        productPrice = (TextView) findViewById(R.id.price);

        productRating = (TextView) findViewById(R.id.rating);
        productFav = (TextView) findViewById(R.id.favorites);
        productDownl = (TextView) findViewById(R.id.dowloands);

        productLongText = (TextView) findViewById(R.id.productLongText);

        //intent
        Intent i = getIntent();

        String sProductTitle = i.getStringExtra(MainActivity.PRODUCT_TITLE);
        String sProductSubtitle = i.getStringExtra(MainActivity.PRODUCT_SUBTITLE);
        int iImageTop = i.getIntExtra(MainActivity.PRODUCT_IMAGETOP, 0);

        int iProductImage = i.getIntExtra(MainActivity.PRODUCT_IMAGE, 0);
        String sProductName = i.getStringExtra(MainActivity.PRODUCT_NAME);
        String sProductDesc = i.getStringExtra(MainActivity.PRODUCT_DESC);
        String sProductPrice = i.getStringExtra(MainActivity.PRODUCT_PRICE);

        String sProductRating = i.getStringExtra(MainActivity.PRODUCT_RATING);
        String sProductFav = i.getStringExtra(MainActivity.PRODUCT_FAVORITES);
        String sProductDownl = i.getStringExtra(MainActivity.PRODUCT_DOWNLOAD);

        String sProductLongText = i.getStringExtra(MainActivity.PRODUCT_LONGTEXT);

        //set
        textTitle.setText(sProductTitle);
        subtitle.setText(sProductSubtitle);
        imageTop.setImageResource(iImageTop);

        productImage.setImageResource(iProductImage);
        productName.setText(sProductName);
        productDesc.setText(sProductDesc);
        productPrice.setText(sProductPrice);

        productRating.setText(sProductRating);
        productFav.setText(sProductFav);
        productDownl.setText(sProductDownl);

        productLongText.setText(sProductLongText);
    }
}