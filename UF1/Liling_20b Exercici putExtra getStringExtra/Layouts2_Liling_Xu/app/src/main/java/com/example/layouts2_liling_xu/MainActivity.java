package com.example.layouts2_liling_xu;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {
    public static String PRODUCT_TITLE = "com.example.layouts2_liling_xu.PRODUCT_TITLE";
    public static String PRODUCT_SUBTITLE= "com.example.layouts2_liling_xu.PRODUCT_SUBTITLE";
    public static String PRODUCT_IMAGETOP = "com.example.layouts2_liling_xu.PRODUCT_IMAGETOP";

    public static String PRODUCT_IMAGE = "com.example.layouts2_liling_xu.PRODUCT_IMAGE";
    public static String PRODUCT_NAME = "com.example.layouts2_liling_xu.PRODUCT_NAME";
    public static String PRODUCT_DESC = "com.example.layouts2_liling_xu.PRODUCT_DESC";
    public static String PRODUCT_PRICE = "com.example.layouts2_liling_xu.PRODUCT_PRICE";

    public static String PRODUCT_RATING = "com.example.layouts2_liling_xu.PRODUCT_RATING";
    public static String PRODUCT_FAVORITES = "com.example.layouts2_liling_xu.PRODUCT_FAVORITES";
    public static String PRODUCT_DOWNLOAD = "com.example.layouts2_liling_xu.PRODUCT_DOWNLOAD";

    public static String PRODUCT_LONGTEXT = "com.example.layouts2_liling_xu.LONGTEXT";



    private CardView cardView11;
    private CardView cardView12;
    private CardView cardView13;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //hook
        cardView11 = (CardView) findViewById(R.id.cardView11);
        cardView12 = (CardView) findViewById(R.id.cardView12);
        cardView13 = (CardView) findViewById(R.id.cardView13);


        cardView11.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String productTitle = "Facebook";
                String productSubtitle = "APP details";//
                int imageTop = R.drawable.lol;//

                int productImage = R.drawable.facebook;
                String productName = "Facebook";
                String productDesc = "Social Network";//
                String productPrice = "Free";//

                String productRating = "Rating 4.2/5";
                String productFav = "Favorites 15 MIL";
                String productDownl = "Size 251.1 MB ";

                String productLongText = "Keeping up with friends is faster and easier than ever. Share updates and photos, engage with friends and Pages, and stay connected to communities important to you.\n" +
                        "\n" +
                        "Features on the Facebook app include:\n" +
                        "\n" +
                        "* Connect with friends and family and meet new people on your social media network\n" +
                        "* Set status updates & use Facebook emoji to help relay what’s going on in your world\n" +
                        "* Share photos, videos, and your favorite memories.\n" +
                        "* Get notifications when friends like and comment on your posts\n" +
                        "* Find local social events, and make plans to meet up with friends\n" +
                        "* Play games with any of your Facebook friends\n" +
                        "* Backup photos by saving them in albums\n" +
                        "* Follow your favorite artists, websites, and companies to get their latest news\n" +
                        "* Look up local businesses to see reviews, operation hours, and pictures\n" +
                        "* Buy and sell locally on Facebook Marketplace\n" +
                        "* Watch live videos on the go";




                //seleccionaros
                Intent intent = new Intent (MainActivity.this, SecondActivity.class);
                intent.putExtra(PRODUCT_TITLE, productTitle);
                intent.putExtra(PRODUCT_SUBTITLE, productSubtitle);
                intent.putExtra(PRODUCT_IMAGETOP, imageTop);

                intent.putExtra(PRODUCT_IMAGE, productImage);
                intent.putExtra(PRODUCT_NAME, productName);
                intent.putExtra(PRODUCT_DESC, productDesc);
                intent.putExtra(PRODUCT_PRICE, productPrice);


                intent.putExtra(PRODUCT_RATING, productRating);
                intent.putExtra(PRODUCT_FAVORITES, productFav);
                intent.putExtra(PRODUCT_DOWNLOAD, productDownl);

                intent.putExtra(PRODUCT_LONGTEXT, productLongText);


                startActivity(intent);
            }
        });

        cardView12.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String productTitle = "Instagram";
                String productSubtitle = "APP details";//
                int imageTop = R.drawable.lol;//

                int productImage = R.drawable.instagram;
                String productName = "Instagram";
                String productDesc = "Social Network";//
                String productPrice = "Free";//

                String productRating = "Rating 4.6/5";
                String productFav = "Favorites 1.2 MILL";
                String productDownl = "Size 150.4 MB ";

                String productLongText = "Bringing you closer to the people and things you love. — Instagram from Facebook\n" +
                        "\n" +
                        "Connect with friends, share what you’re up to, or see what's new from others all over the world. Explore our community where you can feel free to be yourself and share everything from your daily moments to life's highlights.\n" +
                        "\n" +
                        "Express Yourself and Connect With Friends\n" +
                        "\n" +
                        "* Add photos and videos to your INSTA story that disappear after 24 hours, and bring them to life with fun creative tools.\n" +
                        "* Message your friends in Direct. Start fun conversations about what you see on Feed and Stories.\n" +
                        "* Post photos and videos to your feed that you want to show on your profile.\n" +
                        "\n" +
                        "Learn More About Your Interests\n" +
                        "\n" +
                        "* Check out IGTV for longer videos from your favorite INSTA creators.\n" +
                        "* Get inspired by photos and videos from new INSTA accounts in Explore.\n" +
                        "* Discover brands and small businesses, and shop products that are relevant to your personal style.\n";




                //seleccionaros
                Intent intent = new Intent (MainActivity.this, SecondActivity.class);
                intent.putExtra(PRODUCT_TITLE, productTitle);
                intent.putExtra(PRODUCT_SUBTITLE, productSubtitle);
                intent.putExtra(PRODUCT_IMAGETOP, imageTop);

                intent.putExtra(PRODUCT_IMAGE, productImage);
                intent.putExtra(PRODUCT_NAME, productName);
                intent.putExtra(PRODUCT_DESC, productDesc);
                intent.putExtra(PRODUCT_PRICE, productPrice);


                intent.putExtra(PRODUCT_RATING, productRating);
                intent.putExtra(PRODUCT_FAVORITES, productFav);
                intent.putExtra(PRODUCT_DOWNLOAD, productDownl);

                intent.putExtra(PRODUCT_LONGTEXT, productLongText);


                startActivity(intent);
            }
        });

        cardView13.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String productTitle = "TikTok";
                String productSubtitle = "APP details";//
                int imageTop = R.drawable.lol;//

                int productImage = R.drawable.tiktok;
                String productName = "TikTok";
                String productDesc = "Social Network";//
                String productPrice = "Free";//

                String productRating = "Rating 4.8/5";
                String productFav = "Favorites 680 MIL";
                String productDownl = "Size 204.8 MB ";

                String productLongText = "TikTok is THE destination for mobile videos. On TikTok, short-form videos are exciting, spontaneous, and genuine. Whether you’re a sports fanatic, a pet enthusiast, or just looking for a laugh, there’s something for everyone on TikTok. All you have to do is watch, engage with what you like, skip what you don’t, and you’ll find an endless stream of short videos that feel personalized just for you. From your morning coffee to your afternoon errands, TikTok has the videos that are guaranteed to make your day.\n" +
                        "\n" +
                        "We make it easy for you to discover and create your own original videos by providing easy-to-use tools to view and capture your daily moments. Take your videos to the next level with special effects, filters, music, and more.";




                //seleccionaros
                Intent intent = new Intent (MainActivity.this, SecondActivity.class);
                intent.putExtra(PRODUCT_TITLE, productTitle);
                intent.putExtra(PRODUCT_SUBTITLE, productSubtitle);
                intent.putExtra(PRODUCT_IMAGETOP, imageTop);

                intent.putExtra(PRODUCT_IMAGE, productImage);
                intent.putExtra(PRODUCT_NAME, productName);
                intent.putExtra(PRODUCT_DESC, productDesc);
                intent.putExtra(PRODUCT_PRICE, productPrice);


                intent.putExtra(PRODUCT_RATING, productRating);
                intent.putExtra(PRODUCT_FAVORITES, productFav);
                intent.putExtra(PRODUCT_DOWNLOAD, productDownl);

                intent.putExtra(PRODUCT_LONGTEXT, productLongText);


                startActivity(intent);
            }
        });

    }
}