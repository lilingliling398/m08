package com.example.joc_de_cartes_liling;

import androidx.appcompat.app.AppCompatActivity;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

public class MainActivity extends AppCompatActivity {
    private ImageView imageIcon;
    private ImageView fondoNegro;
    private Button button1;
    private Button button2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //hooks
        imageIcon = findViewById(R.id.icono);
        fondoNegro = findViewById(R.id.fondoNegro);
        button1 = findViewById(R.id.player1);
        button2 = findViewById(R.id.players2);

        ObjectAnimator animator1 = ObjectAnimator.ofFloat(imageIcon, "rotation", 0f, 360f);
        ObjectAnimator animator2 = ObjectAnimator.ofFloat(imageIcon, "scaleX", 1f, 10f);
        ObjectAnimator animator3 = ObjectAnimator.ofFloat(imageIcon, "scaleY", 1f, 10f);


        ObjectAnimator aparecer = ObjectAnimator.ofFloat(fondoNegro, "alpha", 0f, 1f);

        ObjectAnimator animator4_1 = ObjectAnimator.ofFloat(button1, "translationX", -1000f, 0f);
        ObjectAnimator animator4_2 = ObjectAnimator.ofFloat(button1, "alpha", 0f, 1f);

        ObjectAnimator animator5_1 = ObjectAnimator.ofFloat(button2, "translationX", 1000f, 0f);
        ObjectAnimator animator5_2 = ObjectAnimator.ofFloat(button2, "alpha", 0f, 1f);

        animator1.setDuration(3000);

        animator2.setStartDelay(3000);
        animator2.setDuration(2100);

        animator3.setStartDelay(3000);
        animator3.setDuration(2100);

        aparecer.setStartDelay(5000);
        aparecer.setDuration(10);

        animator4_1.setStartDelay(5000);
        animator4_1.setDuration(2000);

        animator4_2.setStartDelay(5000);
        animator4_2.setDuration(1000);

        animator5_1.setStartDelay(5000);
        animator5_1.setDuration(2000);

        animator5_2.setStartDelay(5000);
        animator5_2.setDuration(1000);

        AnimatorSet animationSet = new AnimatorSet();
        animationSet.playTogether(animator1, animator2, animator3, aparecer, animator4_1, animator4_2, animator5_1, animator5_2);
        animationSet.start();

        //intent de player
        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                animarBoton();

                intent2Player(false);
            }
        });

        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                animarBoton();

                intent2Player(true);
            }
        });

    }

    private void animarBoton() {
        ObjectAnimator boton1 = ObjectAnimator.ofFloat(button1, "translationX", 0f, -1000f);
        ObjectAnimator boton2 = ObjectAnimator.ofFloat(button2, "translationX", 0f, 1000f);
        boton1.setDuration(2000);
        boton2.setDuration(2000);

        ObjectAnimator boton3 = ObjectAnimator.ofFloat(button1, "translationX", -1000f, 0f);
        ObjectAnimator boton4 = ObjectAnimator.ofFloat(button2, "translationX", 1000f, 0f);

        boton3.setStartDelay(5000);
        boton3.setDuration(2000);

        boton4.setStartDelay(5000);
        boton4.setDuration(2000);

        AnimatorSet animationSet = new AnimatorSet();
        animationSet.playTogether(boton1, boton2, boton3, boton4);
        animationSet.start();
    }

    public void intent2Player(boolean playersDuo) {
        Runnable r = new Runnable() {
            @Override
            public void run() {
                Intent intent;
                if (playersDuo) {
                    intent = new Intent(MainActivity.this, Players2Activity.class);
                } else {
                    intent = new Intent(MainActivity.this, Player1Activity.class);
                }

                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                    startActivity(intent);
                }
            }
        };
        Handler h = new Handler(Looper.getMainLooper());
        h.postDelayed(r, 2000);
    }
}