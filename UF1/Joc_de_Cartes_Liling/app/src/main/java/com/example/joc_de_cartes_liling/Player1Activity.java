package com.example.joc_de_cartes_liling;

import androidx.appcompat.app.AppCompatActivity;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import java.util.ArrayList;
import java.util.List;

public class Player1Activity extends AppCompatActivity {
    private Button bPlay;
    private Button bOneMore;
    private Button bStop;

    //carta por atras
    private ImageView cBack;

    //cartas YOU
    private ImageView cartaB1;
    private ImageView cartaB2;
    private ImageView cartaB3;
    private ImageView cartaB4;
    private ImageView cartaB5;
    private ImageView cartaB6;
    private ImageView cartaB7;
    private ImageView cartaB8;
    private ImageView cartaB9;
    private ImageView cartaB10;
    private ImageView cartaB11;
    private ImageView cartaB12;
    private ImageView cartaB13;
    private ImageView cartaB14;
    private ImageView cartaB15;

    private ImageView cartaY1;
    private ImageView cartaY2;
    private ImageView cartaY3;
    private ImageView cartaY4;
    private ImageView cartaY5;
    private ImageView cartaY6;
    private ImageView cartaY7;
    private ImageView cartaY8;
    private ImageView cartaY9;
    private ImageView cartaY10;
    private ImageView cartaY11;
    private ImageView cartaY12;
    private ImageView cartaY13;
    private ImageView cartaY14;
    private ImageView cartaY15;

    private List<Integer> listaCartas = new ArrayList<>();
    private List<Double> listaValor = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_player1);

        //hook
        bPlay = findViewById(R.id.botonPlay);
        bOneMore = findViewById(R.id.botonOneMore);
        bStop = findViewById(R.id.botonStop);
        cBack = findViewById(R.id.cartaBack);

        cartaB1 = findViewById(R.id.cBank1);
        cartaB2 = findViewById(R.id.cBank2);
        cartaB3 = findViewById(R.id.cBank3);
        cartaB4 = findViewById(R.id.cBank4);
        cartaB5 = findViewById(R.id.cBank5);
        cartaB6 = findViewById(R.id.cBank6);
        cartaB7 = findViewById(R.id.cBank7);
        cartaB8 = findViewById(R.id.cBank8);
        cartaB9 = findViewById(R.id.cBank9);
        cartaB10 = findViewById(R.id.cBank10);
        cartaB11 = findViewById(R.id.cBank11);
        cartaB12 = findViewById(R.id.cBank12);
        cartaB13 = findViewById(R.id.cBank13);
        cartaB14 = findViewById(R.id.cBank14);
        cartaB15 = findViewById(R.id.cBank15);

        cartaY1 = findViewById(R.id.cYou1);
        cartaY2 = findViewById(R.id.cYou2);
        cartaY3 = findViewById(R.id.cYou3);
        cartaY4 = findViewById(R.id.cYou4);
        cartaY5 = findViewById(R.id.cYou5);
        cartaY6 = findViewById(R.id.cYou6);
        cartaY7 = findViewById(R.id.cYou7);
        cartaY8 = findViewById(R.id.cYou8);
        cartaY9 = findViewById(R.id.cYou9);
        cartaY10 = findViewById(R.id.cYou10);
        cartaY11 = findViewById(R.id.cYou11);
        cartaY12 = findViewById(R.id.cYou12);
        cartaY13 = findViewById(R.id.cYou13);
        cartaY14 = findViewById(R.id.cYou14);
        cartaY15 = findViewById(R.id.cYou15);


        //animator boton plat
        ObjectAnimator botonPlayX = ObjectAnimator.ofFloat(bPlay, "scaleX", 0f, 1f);
        ObjectAnimator botonPlayY = ObjectAnimator.ofFloat(bPlay, "scaleY", 0f, 1f);

        botonPlayX.setDuration(1000);
        botonPlayY.setDuration(1000);

        AnimatorSet animationSet = new AnimatorSet();
        animationSet.playTogether(botonPlayX, botonPlayY);
        animationSet.start();

        bPlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bPlay.setClickable(false);
                ObjectAnimator play = ObjectAnimator.ofFloat(bPlay, "alpha", 1f, 0f);
                play.setStartDelay(1000);
                play.setDuration(1000);
                play.start();

                //inicializa las cartas del 1 al 48
                iniciarCartas();

                animarSacarCarta(cartaY1);

/*

                int puntajeYou = 0;


                int index = cartaRandom();
                int cartaSacada = listaCartas.get(index);
                listaCartas.remove(index);
                double valorCarta = listaValor.get(index);
                listaValor.remove(index);


                cartaY1.setImageResource(cartaSacada);
                animarSacarCarta(cartaY1);




                animarBoton();
*/


            }
        });


    }


    public int cartaRandom() {
        return (int) (Math.random() * (listaCartas.size() + 1));
    }

    private void animarSacarCarta(ImageView carta) {


        ObjectAnimator animateX = ObjectAnimator.ofFloat(cBack, "translationX", 0f, carta.getLeft());
        ObjectAnimator animateY = ObjectAnimator.ofFloat(cBack, "translationY", 0f, carta.getTop());
        animateX.setDuration(2000);
        animateY.setDuration(2000);

        ObjectAnimator girarCruz = ObjectAnimator.ofFloat(cBack, "rotationY", 0f, 360f);
        ObjectAnimator alphaCruz = ObjectAnimator.ofFloat(cBack, "alpha", 1f, 0f);
        girarCruz.setStartDelay(2000);
        alphaCruz.setStartDelay(2000);
        girarCruz.setDuration(1000);
        alphaCruz.setDuration(1000);


//        ObjectAnimator cartaX = ObjectAnimator.ofFloat(carta, "translationX", 0f, carta.getTranslationX());
//        ObjectAnimator cartaY = ObjectAnimator.ofFloat(carta, "translationY", 0f,carta.getTranslationY());
//        cartaX.setDuration(3000);
//        cartaY.setDuration(3000);

        ObjectAnimator girarCara = ObjectAnimator.ofFloat(carta, "rotationY", 360f, 0f);
        ObjectAnimator alphaCara = ObjectAnimator.ofFloat(carta, "alpha", 0f, 1f);
        girarCara.setStartDelay(3000);
        alphaCara.setStartDelay(3000);
        girarCara.setDuration(1000);
        alphaCara.setDuration(1000);

        AnimatorSet animationSet = new AnimatorSet();
        animationSet.playTogether(animateX, animateY, girarCruz, alphaCruz, girarCara, alphaCara);
        animationSet.start();

    }

    private void animarBoton() {
        ObjectAnimator boton1 = ObjectAnimator.ofFloat(bOneMore, "translationX", -1000f, 0f);
        ObjectAnimator boton2 = ObjectAnimator.ofFloat(bStop, "translationX", 1000f, 0f);
        boton1.setDuration(2000);
        boton2.setDuration(2000);

        ObjectAnimator boton3 = ObjectAnimator.ofFloat(bOneMore, "translationX", 0f, -1000f);
        boton3.setStartDelay(5000);
        boton3.setDuration(2000);

        ObjectAnimator boton4 = ObjectAnimator.ofFloat(bStop, "translationX", 0f, 1000f);
        boton4.setStartDelay(5000);
        boton4.setDuration(2000);

        AnimatorSet animationSet = new AnimatorSet();
        animationSet.playTogether(boton1, boton2, boton3, boton4);
        animationSet.start();
    }

    public void iniciarCartas() {
        listaCartas.add(R.drawable.clubs01);
        listaCartas.add(R.drawable.clubs02);
        listaCartas.add(R.drawable.clubs03);
        listaCartas.add(R.drawable.clubs04);
        listaCartas.add(R.drawable.clubs05);
        listaCartas.add(R.drawable.clubs06);
        listaCartas.add(R.drawable.clubs07);
        listaCartas.add(R.drawable.clubs08);
        listaCartas.add(R.drawable.clubs09);
        listaCartas.add(R.drawable.clubs10);
        listaCartas.add(R.drawable.clubs11);
        listaCartas.add(R.drawable.clubs12);
        listaCartas.add(R.drawable.cups01);
        listaCartas.add(R.drawable.cups02);
        listaCartas.add(R.drawable.cups03);
        listaCartas.add(R.drawable.cups04);
        listaCartas.add(R.drawable.cups05);
        listaCartas.add(R.drawable.cups06);
        listaCartas.add(R.drawable.cups07);
        listaCartas.add(R.drawable.cups08);
        listaCartas.add(R.drawable.cups09);
        listaCartas.add(R.drawable.cups10);
        listaCartas.add(R.drawable.cups11);
        listaCartas.add(R.drawable.cups12);
        listaCartas.add(R.drawable.golds01);
        listaCartas.add(R.drawable.golds02);
        listaCartas.add(R.drawable.golds03);
        listaCartas.add(R.drawable.golds04);
        listaCartas.add(R.drawable.golds05);
        listaCartas.add(R.drawable.golds06);
        listaCartas.add(R.drawable.golds07);
        listaCartas.add(R.drawable.golds08);
        listaCartas.add(R.drawable.golds09);
        listaCartas.add(R.drawable.golds10);
        listaCartas.add(R.drawable.golds11);
        listaCartas.add(R.drawable.golds12);
        listaCartas.add(R.drawable.swords01);
        listaCartas.add(R.drawable.swords02);
        listaCartas.add(R.drawable.swords03);
        listaCartas.add(R.drawable.swords04);
        listaCartas.add(R.drawable.swords05);
        listaCartas.add(R.drawable.swords06);
        listaCartas.add(R.drawable.swords07);
        listaCartas.add(R.drawable.swords08);
        listaCartas.add(R.drawable.swords09);
        listaCartas.add(R.drawable.swords10);
        listaCartas.add(R.drawable.swords11);
        listaCartas.add(R.drawable.swords12);

        for (int i = 0; i < 4; i++) {
            listaValor.add(1.0);
            listaValor.add(2.0);
            listaValor.add(3.0);
            listaValor.add(4.0);
            listaValor.add(5.0);
            listaValor.add(6.0);
            listaValor.add(7.0);
            listaValor.add(0.5);
            listaValor.add(0.5);
            listaValor.add(0.5);
            listaValor.add(0.5);
            listaValor.add(0.5);
        }


    }

}


