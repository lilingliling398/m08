package com.example.liling_examemuf1;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageView;

public class PruebaActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_prueba);

        Intent i = getIntent();
        int image = i.getIntExtra(DashboardActivity.PRODUCT_IMAGE, 0);

        ImageView imagen = findViewById(R.id.aaa);
        imagen.setImageResource(image);
    }
}