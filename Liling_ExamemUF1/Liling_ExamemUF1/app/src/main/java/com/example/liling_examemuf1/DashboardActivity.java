package com.example.liling_examemuf1;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class DashboardActivity extends AppCompatActivity {

    //producto
    public static String PRODUCT_TITOL = "com.example.liling_examemuf1.PRODUCT_TITOL";
    public static String PRODUCT_IMAGE = "com.example.liling_examemuf1.PRODUCT_IMAGE";
    public static String NUM = "com.example.liling_examemuf1.NUM";

    private CardView cardView1;
    private CardView cardView2;
    private CardView cardView3;
    private CardView cardView4;
    private CardView cardView5;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);

        //Hooks
        cardView1 = findViewById(R.id.card1);
        cardView2 = findViewById(R.id.card2);
        cardView3 = findViewById(R.id.card3);
        cardView4 = findViewById(R.id.card4);
        cardView5 = findViewById(R.id.card5);

        cardView1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String productTitol = "Refugi Josep Maria Blanc";
                int imageTop = R.drawable.foto1;

                //seleccionaros
                Intent intent = new Intent(DashboardActivity.this, ReservaActivity.class);
                intent.putExtra(PRODUCT_TITOL, productTitol);
                intent.putExtra(PRODUCT_IMAGE, imageTop);
                intent.putExtra(NUM, 1);

                startActivity(intent);
            }
        });

        cardView2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String productTitol = "Refugi Cap de Llauset";
                int imageTop = R.drawable.foto2;

                //seleccionaros
                Intent intent = new Intent(DashboardActivity.this, ReservaActivity.class);
                intent.putExtra(PRODUCT_TITOL, productTitol);
                intent.putExtra(PRODUCT_IMAGE, imageTop);
                intent.putExtra(NUM, 2);


                startActivity(intent);
            }
        });

        cardView3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String productTitol = "Refugi Ventosa i Clavell";
                int imageTop = R.drawable.foto3;

                //seleccionaros
                Intent intent = new Intent(DashboardActivity.this, ReservaActivity.class);
                intent.putExtra(PRODUCT_TITOL, productTitol);
                intent.putExtra(PRODUCT_IMAGE, imageTop);
                intent.putExtra(NUM, 3);


                startActivity(intent);
            }
        });

        cardView4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String productTitol = "Refugi Amitges";
                int imageTop = R.drawable.foto4;

                //seleccionaros
                Intent intent = new Intent(DashboardActivity.this, ReservaActivity.class);
                intent.putExtra(PRODUCT_TITOL, productTitol);
                intent.putExtra(PRODUCT_IMAGE, imageTop);
                intent.putExtra(NUM, 4);


                startActivity(intent);
            }
        });

        cardView5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String productTitol = "Refugi Josep Maria Montfort";
                int imageTop = R.drawable.foto5;

                //seleccionaros
                Intent intent = new Intent(DashboardActivity.this, ReservaActivity.class);
                intent.putExtra(PRODUCT_TITOL, productTitol);
                intent.putExtra(PRODUCT_IMAGE, imageTop);
                intent.putExtra(NUM, 5);


                startActivity(intent);
            }
        });

    }
}