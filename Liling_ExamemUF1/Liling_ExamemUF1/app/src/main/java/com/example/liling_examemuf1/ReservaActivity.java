package com.example.liling_examemuf1;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SwitchCompat;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class ReservaActivity extends AppCompatActivity {

    private RadioGroup radioGroup;
    private RadioButton radioBt1;
    private RadioButton radioBt2;
    private RadioButton radioBt3;
    private RadioButton radioBt4;
    private SwitchCompat switchCompat;

    DatePickerDialog pickerEntrada;
    DatePickerDialog pickerSortida;
    EditText textDateEntrada;
    EditText textDateSortida;


    private ImageView imageTop;
    private Spinner spinnerString;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reserva);

        //hook
        radioGroup = findViewById(R.id.radioGroup);
        radioBt1 = findViewById(R.id.radioButton);
        radioBt2 = findViewById(R.id.radioButton2);
        radioBt3 = findViewById(R.id.radioButton3);
        radioBt4 = findViewById(R.id.radioButton4);
        switchCompat = findViewById(R.id.switch1);

        textDateEntrada = findViewById(R.id.editTextDate);
        textDateSortida = findViewById(R.id.editTextDate2);

        imageTop = findViewById(R.id.imageView4);

        spinnerString = findViewById(R.id.spinnerString);

        //intent
        Intent i = getIntent();
        int sProductImage = i.getIntExtra(DashboardActivity.PRODUCT_IMAGE, 0);
        String titulo = i.getStringExtra(DashboardActivity.PRODUCT_TITOL);
        int num = i.getIntExtra(DashboardActivity.NUM, 0);

        //set
        imageTop.setImageResource(sProductImage);

        //Creamos ArrayList de tipo String
        List<String> casas = new ArrayList<>();
        casas.add("Selecciona curs");
        casas.add("Refugi Josep Maria Blanc");
        casas.add("Refugi Cap de Llauset");
        casas.add("Refugi Ventosa i Clavell");
        casas.add("Refugi Amitges");
        casas.add("Refugi Josep Maria Montfort");

        //Creamos un Adapter con este ArrayList y se lo asignamos al Spinner
        ArrayAdapter<String> casaAdapter = new ArrayAdapter<String>(this, R.layout.support_simple_spinner_dropdown_item, casas);
        spinnerString.setAdapter(casaAdapter);

        spinnerString.setSelection(num);
        spinnerString.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String selectedCondition = parent.getItemAtPosition(position).toString();
                Toast.makeText(ReservaActivity.this, selectedCondition, Toast.LENGTH_SHORT).show();

                int image = sProductImage;
                switch (selectedCondition) {
                    case "Refugi Josep Maria Blanc":
                        image = R.drawable.foto1;
                        break;
                    case "Refugi Cap de Llauset":
                        image = R.drawable.foto2;
                        break;
                    case "Refugi Ventosa i Clavell":
                        image = R.drawable.foto3;
                        break;
                    case "Refugi Amitges":
                        image = R.drawable.foto4;
                        break;
                    case "Refugi Josep Maria Montfort":
                        image = R.drawable.foto5;
                        break;
                }
                imageTop.setImageResource(image);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                Toast.makeText(ReservaActivity.this, "Nothing Selected", Toast.LENGTH_SHORT).show();
            }
        });


        radioBt1.setEnabled(false);
        radioBt2.setEnabled(false);
        radioBt3.setEnabled(false);
        radioBt4.setEnabled(false);

        //switch
        if (switchCompat != null) {
            switchCompat.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (isChecked) {
                        Toast.makeText(ReservaActivity.this, "Activat", Toast.LENGTH_SHORT).show();
                        radioBt1.setEnabled(true);
                        radioBt2.setEnabled(true);
                        radioBt3.setEnabled(true);
                        radioBt4.setEnabled(true);

                    } else {
                        Toast.makeText(ReservaActivity.this, "Desactivat", Toast.LENGTH_SHORT).show();
                        radioBt1.setEnabled(false);
                        radioBt2.setEnabled(false);
                        radioBt3.setEnabled(false);
                        radioBt4.setEnabled(false);
                        radioBt1.setChecked(false);
                        radioBt2.setChecked(false);
                        radioBt3.setChecked(false);
                        radioBt4.setChecked(true);
                    }
                }

            });

        }

        //fecha
        textDateEntrada.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar cldr = Calendar.getInstance();
                int day = cldr.get(Calendar.DAY_OF_MONTH);
                int month = cldr.get(Calendar.MONTH);
                int year = cldr.get(Calendar.YEAR);

                pickerEntrada = new DatePickerDialog(ReservaActivity.this, android.R.style.Theme_Holo_Light_Dialog, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        textDateEntrada.setText(dayOfMonth + "/" + (monthOfYear + 1) + "/" + year);
                    }
                }, year, month, day);
                pickerEntrada.show();

            }
        });

        textDateSortida.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar cldr = Calendar.getInstance();
                int day = cldr.get(Calendar.DAY_OF_MONTH);
                int month = cldr.get(Calendar.MONTH);
                int year = cldr.get(Calendar.YEAR);

                pickerSortida = new DatePickerDialog(ReservaActivity.this, android.R.style.Theme_Holo_Light_Dialog, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        textDateSortida.setText(dayOfMonth + "/" + (monthOfYear + 1) + "/" + year);
                    }
                }, year, month, day);
                pickerSortida.show();

            }
        });


    }
}