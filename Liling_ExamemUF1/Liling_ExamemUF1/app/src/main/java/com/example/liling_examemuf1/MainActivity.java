 package com.example.liling_examemuf1;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.app.ActivityOptions;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Pair;
import android.view.View;
import android.widget.CalendarView;
import android.widget.ImageView;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    private static final long SPLASH_SCREEN = 4000;
    private ImageView imageView2;
    private ImageView imageView1;
    private CardView cardViewLogo;

    ObjectAnimator objectAnimator1;
    ObjectAnimator objectAnimator2;
    ObjectAnimator objectAnimator3;
    ObjectAnimator objectAnimator4;
    ObjectAnimator objectAnimator5;

    private ImageView imageApp;
    private TextView textName;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //hooks
        imageView2 = (ImageView) findViewById(R.id.imageView2);
        imageView1 = (ImageView) findViewById(R.id.imageView);

        cardViewLogo = (CardView) findViewById(R.id.textoLogo);

        textName = findViewById(R.id.textView3);


        //animacion logo1
        objectAnimator1 = ObjectAnimator.ofFloat(imageView1, "translationX", -1000f, 0f);
        objectAnimator1.setDuration(2000);

        objectAnimator2 = ObjectAnimator.ofFloat(imageView1, "alpha", 0f, 1f);
        objectAnimator2.setDuration(2000);

        //animacion txto
        objectAnimator3 = ObjectAnimator.ofFloat(cardViewLogo, "translationX", 1000f, 0f);
        objectAnimator3.setDuration(2000);

        objectAnimator4 = ObjectAnimator.ofFloat(cardViewLogo, "alpha", 0f, 1f);
        objectAnimator4.setDuration(2000);

        //animacion texto
        objectAnimator5 = ObjectAnimator.ofFloat(imageView2, "alpha", 0f, 1f);
        objectAnimator5.setStartDelay(2000);
        objectAnimator5.setDuration(2000);


        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.playTogether(objectAnimator1, objectAnimator2, objectAnimator3, objectAnimator4, objectAnimator5);
        animatorSet.start();

        Runnable r = new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(MainActivity.this, DashboardActivity.class);
                Pair[] pairs = new Pair[2];
                pairs[0] = new Pair<View, String>(imageView1, "image_origen");
                pairs[1] = new Pair<View, String>(textName, "textapp_origen");
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                    ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation(MainActivity.this, pairs);
                    startActivity(intent, options.toBundle());
                }
            }
        };
        Handler h = new Handler(Looper.getMainLooper());
        h.postDelayed(r, SPLASH_SCREEN);

    }
}